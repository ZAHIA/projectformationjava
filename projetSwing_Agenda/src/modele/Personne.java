package modele;

import java.io.Serializable;
import java.util.Objects;

public class Personne implements Comparable<Personne>, Serializable {

    //----------------------------- attributs d'instance ---------------------
    private String nom;
    private String prenom;
    private String region;

    //--------------------------------- Getter Setter ------------------------
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = (nom==null || nom.toString() == "" ?"INCONNU":nom.toUpperCase());
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = (prenom== null || prenom == ""?"INCONNU":prenom.toUpperCase());
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = (region==null?"INCONNU":region.toUpperCase());
    }

    //------------------------------- constructeur ----------------------------
    public Personne(String nom, String prenom, String region) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setRegion(region);
    }

    //-------------------------------- tostring --------------------------------
    @Override
    public String toString() {
        return nom;
    }

    //--------------------------------- equals ----------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personne)) return false;
        Personne personne = (Personne) o;
        return nom.equals(personne.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    //---------------------------------- compare to -----------------------------
    @Override
    public int compareTo(Personne autre) {
        return this.nom.compareTo(autre.nom);
    }
}
