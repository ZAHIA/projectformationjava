package vues;

import modele.Personne;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

public class noName {
    private JPanel rootPannel;
    private JButton ajouterButton;
    private JButton modifierButton;
    private JButton mailingButton;
    private JTextArea textAreaInfoPersonne;
    private JList<Personne> listPersonne;

    private DefaultListModel<Personne> modele = new DefaultListModel<>();
    private AjoutDlg ajoutDlg = null;
    private MailingDlg mailingDLG = null;

    private void initModele(){

        try {
            JFileChooser fileChooser = new JFileChooser("D:/POP_java/");
            int status = fileChooser.showOpenDialog(null);
            if (status == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fileChooser.getSelectedFile();
                FileInputStream f = new FileInputStream(selectedFile);
                ObjectInputStream out = new ObjectInputStream(f);

                this.modele = (DefaultListModel) out.readObject();
                out.close();
                return;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        /*this.modele = new DefaultListModel<>();
        modele.addElement(new Personne("LEFEBVRE", "MICHAEL", "nord"));
        modele.addElement(new Personne("BOUCHEZ", "MARC", "VAR"));
        modele.addElement(new Personne("PEPETTE", "lili", "ILE DE FRANCE"));
        modele.addElement(new Personne("MINOU", "zizi", "PAS DE CALAIS"));*/
    }


    public noName() {
        //lier le modele
        initModele();
        this.listPersonne.setModel(modele);
        //gestion evenement
        ajouterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(ajoutDlg == null){
                    ajoutDlg = new AjoutDlg();
                    ajoutDlg.pack();
                }
                Personne pers = new Personne(null, null, null);
                if (ajoutDlg.ouvrirDlg(pers, true)){
                    if (! modele.contains(pers)) //refus des doublons
                        modele.addElement(pers);
                }
                textAreaInfoPersonne.setText("");
                modifierButton.setEnabled(false);
            }
        });

        //---------------------------------evenement sur la liste --------------------------------
        listPersonne.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                Personne pers = listPersonne.getSelectedValue();

                //textAreaInfoPersonne.setText("");
                if(pers==null) return;
                textAreaInfoPersonne.setText("nom : " + pers.getNom());
                textAreaInfoPersonne.append("\nprenom : " + pers.getPrenom());
                textAreaInfoPersonne.append("\nregion : " + pers.getRegion());
                //rend visible le bouton modifier
                modifierButton.setEnabled(true);
            }
        });

        modifierButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(ajoutDlg == null){
                    ajoutDlg = new AjoutDlg();
                    ajoutDlg.pack();
                }
                Personne pers = listPersonne.getSelectedValue();
                Personne nouv = new Personne(pers.getNom(),pers.getPrenom(),pers.getRegion());
                if (ajoutDlg.ouvrirDlg(nouv, false)){
                    listPersonne.clearSelection();
                    modele.removeElement(pers);
                    modele.addElement(nouv);
                }
                textAreaInfoPersonne.setText("");
                modifierButton.setEnabled(false);
            }
        });

        mailingButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if (mailingDLG == null){
                    mailingDLG= new MailingDlg();
                    mailingDLG.pack();
                }
                mailingDLG.ouvrir(modele);
            }
        });
    }

    //------------------------------------- main ------------------------------
    public static void main(String[] args) {
        JFrame frame = new JFrame("noName");
        frame.setContentPane(new noName().rootPannel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
