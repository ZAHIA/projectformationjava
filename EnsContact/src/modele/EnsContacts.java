package modele;

import java.util.Enumeration;
import java.util.Hashtable;

public class EnsContacts {
    //attributs d'instance
    private Hashtable<String, Societe> ensSoc = new Hashtable<>();

    public void ajouter(String nomClient, String prenomClient, String nomSoc){
        //création d'un objet client
        Client c1 = new Client(nomClient, prenomClient, nomSoc);
        //récuperation du nom de la societe
        String cle = c1.getSociete();
        //créartion de lla societe
        creerSociete(cle);
        //ajouter le client
        Societe soc = this.ensSoc.get(cle);
        soc.ajouterClient(c1);
    }

    private void creerSociete(String nom){
        String cle = (nom==null ? "CORA" : nom.toUpperCase());
        if (this.ensSoc.containsKey(cle)) return;

        this.ensSoc.put(cle, new Societe(cle));
    }

    public void afficher(){
        for (String nomSoc : this.ensSoc.keySet()){
            System.out.println(nomSoc);
        }
    }

    public void afficherBis(){
        Enumeration<String> enumCle = this.ensSoc.keys();
        while (enumCle.hasMoreElements()){
            String cle = enumCle.nextElement();
            System.out.println(cle);
        }
    }

    //to string


    @Override
    public String toString() {
        return "EnsContacts{" +
                "ensSoc=" + ensSoc +
                '}';
    }

    public void afficherTout(){
        /*Enumeration<String> enumCle = this.ensSoc.keys();
        while (enumCle.hasMoreElements()){
            String cle = enumCle.nextElement();
            System.out.println(cle);

            Societe soc = this.ensSoc.get(cle);
            Enumeration<Client> enumc1 = soc.getEns().elements();
            while (enumc1.hasMoreElements()){
                Client cl = enumc1.nextElement();
                System.out.println("\t"+cl.toString());
            }
        }*/
        for (String cle: this.ensSoc.keySet()){
            System.out.println(cle);
            Societe soc = this.ensSoc.get(cle);

            for (Client cl: soc.getEns()){
                System.out.println("\t"+cl.toString());
            }
        }
    }

    //constructeur


    public static void main(String[] args) {

        EnsContacts ec1 = new EnsContacts();
        ec1.ajouter("Lefebvre","Michael", "pop");
        ec1.ajouter("Bouchez","marc", "pop");
        ec1.ajouter("tutu", "titi", "pop");
        ec1.ajouter("Qouirez", "Amélie", "Dentelière");
        ec1.ajouter("dede","paul","Dentelière");
        ec1.ajouter(null,null,null);
        ec1.afficher();
        ec1.afficherTout();


    }
}
