package modele;

import java.util.Objects;
import java.util.Vector;

public class Client {
    //variable instance
    private String nom;
    private String prenom;
    private String societe;

    //getter setter


    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {

        this.nom = (nom==null?"GATOR":nom.toUpperCase());
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = (prenom==null? "NATHALIE": prenom.toUpperCase());
    }

    public String getSociete() {
        return societe;
    }

    public void setSociete(String societe) {
        this.societe = (societe==null?"SOCIETE":societe.toUpperCase());
    }

    //equals


    //constructeur

    public Client(String nom, String prenom, String societe) {
        this.setNom(nom);
        this.setPrenom(prenom);
        this.setSociete(societe);
    }

    protected Client() {
        this(null,null,null);
    }

    //equals & hashcode

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return nom.equals(client.nom) &&
                prenom.equals(client.prenom) &&
                societe.equals(client.societe);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom, societe);
    }


    //to string

    @Override
    public String toString() {
        return "Client{" +
                "nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", societe='" + societe + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Client c1 = new Client();
        Client c2 = new Client("Lefebvre", "Michael", "Popschool");
        Client c3 = new Client("Lefebvre", "Michael", "Popschool");
        System.out.println(c1);
        System.out.println(c2);

        //EGALITE DES OBJETS
        Vector<Client> ens = new Vector<>();                //notation diamand
        ens.addElement(c2);                                //ou ens.add(c2); //en java2

        if(ens.contains(c2))
            System.out.println("YES");
        if(ens.contains(c3))
            System.out.println("YES");
        else
            System.out.println("OUPS!!!");
    }
}
