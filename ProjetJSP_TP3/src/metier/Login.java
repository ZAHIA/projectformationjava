package metier;

public class Login {
    //attributs d'instance
    private String nom = "";
    private String password = "";

    //GETTER SETTER
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //regle metier
    public boolean isConnu(){
        return (nom.equals("michael") && password.equals("tiger"));
    }

    //constructeur
    public Login() {
    }
}
