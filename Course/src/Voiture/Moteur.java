package Voiture;

public class Moteur {
    //variable d'instance
    private int puissance;
    private char carburant;

    //getter / setter
    public int getPuissance() {
        return puissance;
    }

    public void setPuissance(int puissance) {
        this.puissance = (puissance <= 1000 ? 1000 : puissance);
    }

    public char getCarburant() {
        return carburant;
    }

    public void setCarburant(char carburant) {
        this.carburant = (carburant != 'E' && carburant != 'D' ? 'E' : carburant);
    }

    //constructeur

    public Moteur() {
        setCarburant('E');
        setPuissance(1000);
    }

    public Moteur(int puissance, char carburant) {
        this.puissance = puissance;
        this.carburant = carburant;
    }

    @Override
    public String toString() {
        return "Moteur{" +
                "puissance=" + puissance +
                ", carburant=" + carburant +
                '}';
    }

    //test unitaire
    public static void main(String[] args) {
        Moteur m1 = new Moteur(1000, 'D');
        Moteur m2 = new Moteur(1000, 'F');

        System.out.println(m1);
        System.out.println(m2);
    }
}

