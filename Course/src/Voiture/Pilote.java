package Voiture;

public class Pilote extends Personne{
    //attributs d'instance
    private String sponsor;

    //getter setter


    public String getSponsor() {
        return sponsor;
    }

    private void setSponsor(String sponsor) {
        this.sponsor = (sponsor==null
                ? "pilote"
                : sponsor.toUpperCase());
    }

    //constructeur

    public Pilote(String nom, String prenom, String sponsor) {
        super(nom,prenom);
        this.setSponsor(sponsor);
    }

    public Pilote(String nom, String prenom) {
        this(nom,prenom, null);
        this.sponsor = null;
    }

    public Pilote() {
        this.sponsor = "laposte";
    }

    @Override
    protected boolean estCompatible(Voiture v){
        if (v==null) return false;
        //return (v instanceof Rallye);
        return v.getClass()== Rallye.class;
    }

    //to string

    @Override
    public String toString() {
        return super.toString() +"\n\t\t" +
                "sponsor='" + sponsor + '\'' +
                '}';
    }

    public static void main(String[] args) {
        Pilote p1 = new Pilote("tutu", "thomas", "laposte");
        System.out.println(p1);
        Voiture v1 = new Rallye("REnault", 1650, 'E',5);
        p1.affecter(v1);
        System.out.println(p1);
    }
}
