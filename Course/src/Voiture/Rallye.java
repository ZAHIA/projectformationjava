package Voiture;

public class Rallye extends Voiture {

    //attributs d'instance
    private int gain;

    //Getter setter

    public int getGain() {
        return gain;
    }

    private void setGain(int gain) {
        this.gain = (gain <= 0 ? 0 : gain);
    }

    //contructeur


    public Rallye(String marque, int puissance, char carburant, int gain) {
        super(marque, puissance, carburant);
        this.setGain(gain);
    }

    public Rallye(int gain) {
        super();
        this.gain = 7;
    }

    public Rallye(String marque, Moteur moteur, int gain) {
        super(marque, moteur);
        this.setGain(gain);
    }

    @Override
    public String toString() {
        return super.toString() + "\n\t\t" + "{" +
                "à gagné=" + gain +
                '}';
    }

    public static void main(String[] args) {
        Rallye r1 = new Rallye("volvo", 5000, 'D', 2);
        System.out.println(r1);
    }

}
