package dto;

public class MedInfo {
    private String nom;
    private float salaire;

    public MedInfo(String nom, float salaire) {
        this.nom = nom;
        this.salaire = salaire;
    }

    @Override
    public String toString() {
        return "MedInfo{" +
                "nom='" + nom + '\'' +
                ", salaire='" + salaire + '\'' +
                '}';
    }
}
