package modele;

import dto.MedInfo;

import javax.persistence.*;
import java.util.List;

public class Main {

    private static EntityManagerFactory f = Persistence.createEntityManagerFactory("Hopital_PU");


    private void init() {
        String query = "SELECT s FROM Service s ORDER BY s.nom DESC ";
        String query1 = "SELECT m FROM Medecin m ORDER BY m.nom";

        Service serv1 = new Service("Cardiologie", "Bat A, 1er étage");
        Service serv2 = new Service("Gériatrice", "Bat B, rez-de-chaussée");
        Service serv3 = new Service("Urgence", "Bat A");

        Medecin med1 = new Medecin("Marc", "Jean", 1900);
        Medecin med2 = new Medecin("Bif", "Dof", 1800);
        Medecin med3 = new Medecin("Pif", "Paf", 2000);
        Medecin med4 = new Medecin("Tic", "Tac", 3000);
        Medecin med5 = new Medecin("Loc", "Luc", 2400);
        Medecin med6 = new Medecin("Toc", "Toc", 1600);

        serv1.addMedecin(med1);
        serv1.addMedecin(med2);
        serv2.addMedecin(med3);
        serv2.addMedecin(med4);
        serv3.addMedecin(med5);
        serv3.addMedecin(med6);

        med1.setMgr(med2);
        med3.setMgr(med4);
        med5.setMgr(med6);

        serv1.setResponsable(med2);

        Adresse ad = new Adresse(10, "rue du bid", "valenciennes");

        Malade mal1 = new Malade("tur", "baf", ad);
        Malade mal2 = new Malade("tur", "baf", new Adresse(666, "rue des enfers", "paradis"));

        EntityManagerFactory emf =
        Persistence.createEntityManagerFactory("Hopital_PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(serv1);
            em.persist(serv2);
            em.persist(serv3);

            em.persist(med1);
            em.persist(med2);
            em.persist(med3);
            em.persist(med4);
            em.persist(med5);
            em.persist(med6);

            em.persist(mal1);
            em.persist(mal2);

            em.getTransaction().commit();

            List<Service> liste = em.createQuery(query, Service.class).getResultList();

            for (Service s : liste) {
                System.out.println("" + s);
            }
            List<Medecin> liste1 = em.createQuery(query1, Medecin.class).getResultList();
            for (Medecin m : liste1) {
                System.out.println("" + m);
            }

        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        } finally {
            em.close();
        }
    }

    public void test5_2(String service) {

        String sel = "select new dto.MedInfo(m.nom, m.salaire) from Medecin m " +
                " Where upper(m.service.nom) like :pat";

        String pat = "%" + service + "%";

        EntityManager em = f.createEntityManager();
        try {
            TypedQuery<MedInfo> query = em.createQuery(sel, MedInfo.class);
            query.setParameter("pat", pat);
            List<MedInfo> liste = query.getResultList();
            for (MedInfo med : liste) {
                System.out.println("" + med);
            }

        } finally {
            em.close();
        }
    }


        public void test5_1(String service) {

            String sel = "select m from Medecin m " +
                    " Where upper(m.service.nom) like :pat";

            String pat = "%" + service + "%";

            EntityManager em = f.createEntityManager();
            try {
                TypedQuery<Medecin> query = em.createQuery(sel, Medecin.class);
                query.setParameter("pat", pat);
                List<Medecin> liste = query.getResultList();
                for (Medecin med : liste) {
                    System.out.println("" + med);
                }

            }finally {
                em.close();
            }
    }
    public void test5_3(String service) {
        EntityManager em = f.createEntityManager();
        try {
            TypedQuery<MedInfo> query = em.createNamedQuery("Medecin.findNomAndSalaire", MedInfo.class);
            query.setParameter("nom", service.toUpperCase());
            List<MedInfo> liste = query.getResultList();
            for (MedInfo med : liste) {
                System.out.println("" + med);
            }

        } finally {
            em.close();
        }
    }

    public void test5_4() {
        EntityManager em = f.createEntityManager();
        try {
            TypedQuery<Medecin> query = em.createNamedQuery("Medecin.findAll", Medecin.class);
            List<Medecin> liste = query.getResultList();
            for (Medecin med : liste) {
                System.out.println("" + med);
            }
            //changement du salaire + 5%
            em.getTransaction().begin(); //pour modifier la base
            for (Medecin med : liste) {
                med.setSalaire(med.getSalaire()*1.05f);
            }
            em.getTransaction().commit();
            //changement du salaire fait
            System.out.println("-----------------------APRES ----------------");
            for (Medecin med : liste) {
                System.out.println("" + med);
            }
        } finally {
            em.close();
        }
    }

    public void test5_5() {
        EntityManager em = f.createEntityManager();
        try {
            TypedQuery<Medecin> query = em.createNamedQuery("Medecin.findAll", Medecin.class);
            List<Medecin> liste = query.getResultList();
            for (Medecin med : liste) {
                System.out.println("" + med);
            }
            //changement du salaire + 5%
            em.getTransaction().begin();

            String ordre =
                    "update Medecin m " +
                            " set m.salaire = 2400 ";
            Query q  = em.createQuery(ordre);

            int nbEntiteModifiees = q.executeUpdate();

            em.getTransaction().commit();
            //changement du salaire fait
            System.out.println("-----------------------APRES ----------------");
            TypedQuery<Medecin> query2 = em.createNamedQuery("Medecin.findAll", Medecin.class);
            List<Medecin> liste2 = query2.getResultList();
            for (Medecin med : liste2) {
                System.out.println("" + med);
            }
        } finally {
            em.close();
        }
    }

    public void test5_5_1() {
        EntityManager em = f.createEntityManager();
        try {
            TypedQuery<Medecin> query = em.createNamedQuery("Medecin.findAll", Medecin.class);
            List<Medecin> liste = query.getResultList();
            for (Medecin med : liste) {
                System.out.println("" + med);
            }
            //changement du salaire + 5%
            em.getTransaction().begin();

            String ordre =
                    "update Medecin m " +
                            " set m.salaire = 2300 ";
            Query q  = em.createQuery(ordre);

            int nbEntiteModifiees = q.executeUpdate();

            em.getTransaction().commit();
            //changement du salaire fait
            System.out.println("-----------------------APRES ----------------");

            for (Medecin med : liste) {
                em.refresh(med);
                System.out.println("" + med);
            }
        } finally {
            em.close();
        }
    }

    public static void main(String[] args) {

        Main t = new Main();
        t.init();
        //t.test5_5_1();
    }
}
