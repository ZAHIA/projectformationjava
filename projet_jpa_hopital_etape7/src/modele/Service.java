package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
public class Service implements Serializable {
    private static final long serialVersion = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, unique = true, length = 30)
    private String nom;
    @Column(nullable = false, length = 40)
    private String situation;


    // association one to many ... entre les tables
    @OneToMany(mappedBy = "service")
    private Set<Medecin> medecins = new HashSet<>();

    @OneToOne
    @JoinColumn(name="responsable_id")
    private Medecin responsable;


    //indispenssable mais ser inaccessible


    public Service() {
        this.nom = "nouveau nom";
        this.situation = "nouvelle situation";
    }

    public Service(String nom, String situation) {
        setNom(nom);
        setSituation(situation);
    }

    public int getId() {
        return id;
    }



    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom.toUpperCase();
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public Set<Medecin> getMedecins() {
        return medecins;
    }

    public void setMedecins(Set<Medecin> medecins){
        this.medecins=medecins;
    }

    public void addMedecin(Medecin med){
        if (med==null)return;

        if (med.getService()!=null) {
            if (med.getService().equals(this))
                return;
            else
                med.getService().getMedecins().remove(med);
        }
        medecins.add(med);
        med.setService(this);
    }

    public Medecin getResponsable(){
        return responsable;
    }

    public void setResponsable(Medecin responsable){
        this.responsable= responsable;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Service)) return false;
        Service service = (Service) o;
        return nom.equals(service.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }


    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", situation='" + situation + '\'' +
                ", responsable=" + (responsable==null?", sans responsable" : "dirigé par :" + responsable.getNom()) +
                '}';
    }

    public static void main(String[] args) {
        String query = "SELECT s FROM Service s ORDER BY s.nom DESC ";
        String query1 = "SELECT p FROM Personne p ORDER BY p.nom";

        Service serv1 = new Service("Cardiologie", "Bat A, 1er étage");
        Service serv2 = new Service("Gériatrice", "Bat B, rez-de-chaussée");
        Service serv3 = new Service("Urgence", "Bat A");

        Medecin med1 = new Medecin("Marc", "Jean", 1900);
        Medecin med2 = new Medecin("Bif", "Dof", 1800);
        Medecin med3 = new Medecin("Pif", "Paf", 2000);
        Medecin med4 = new Medecin("Tic", "Tac", 3000);
        Medecin med5 = new Medecin("Loc", "Luc", 2400);
        Medecin med6 = new Medecin("Toc", "Toc", 1600);

        serv1.addMedecin(med1);
        serv1.addMedecin(med2);
        serv2.addMedecin(med3);
        serv2.addMedecin(med4);
        serv3.addMedecin(med5);
        serv3.addMedecin(med6);

        med1.setMgr(med2);
        med3.setMgr(med4);
        med5.setMgr(med6);

        serv1.setResponsable(med2);

        Adresse ad = new Adresse( 10 , "rue du bid", "valenciennes");

        Personne mal1 = new Malade("tur", "baf", ad);
        Personne mal2 = new Malade("bip", "bop", new Adresse(666, "rue des enfers", "paradis"));

        EntityManagerFactory emf=
                Persistence.createEntityManagerFactory("Hopital_PU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
            em.persist(serv1);
            em.persist(serv2);
            em.persist(serv3);

            em.persist(med1);
            em.persist(med2);
            em.persist(med3);
            em.persist(med4);
            em.persist(med5);
            em.persist(med6);

            em.persist(mal1);
            em.persist(mal2);

            em.getTransaction().commit();

            List<Service> liste = em.createQuery(query, Service.class).getResultList();

            for (Service s : liste){
                System.out.println(""+s);
            }
            List<Personne> liste1 = em.createQuery(query1, Personne.class).getResultList();
            for (Personne m : liste1){
                System.out.println(""+m);
            }

        }catch (Exception e){
            e.printStackTrace();
            em.getTransaction().rollback();
        }finally {
            em.close();
        }
    }
}
