package util;

import java.io.Serializable;
import java.util.Date;

public class InfoCB implements Serializable {
    private String numeroCarte="";
    private Date dateExpirationCarte=new Date(System.currentTimeMillis());

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public Date getDateExpirationCarte() {
        return dateExpirationCarte;
    }

    public void setDateExpirationCarte(Date dateExpirationCarte) {
        this.dateExpirationCarte = dateExpirationCarte;
    }

    public InfoCB() {
    }

    public InfoCB(String numeroCarte, Date dateExpirationCarte) {
        this.numeroCarte = numeroCarte;
        this.dateExpirationCarte = dateExpirationCarte;
    }
}
