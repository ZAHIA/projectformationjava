package controleur;

import modele.Client;
import modele.Item;
import service.ServiceCart;
import service.ServiceItem;
import util.Achat;
import util.CardExpiredException;
import util.InfoCB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.persistence.criteria.CriteriaBuilder;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.*;

@ManagedBean(name = "cartCtrl")
@SessionScoped
public class CartControleur implements Serializable {
    // donnees en session
    private Client client;
    private List<Achat> panier = null;
    private InfoCB infoCB = null;

    private String numeroCB;
    private java.util.Date dateExpirationCB;

    public String getNumeroCB() {
        return numeroCB;
    }

    public void setNumeroCB(String numeroCB) {
        this.numeroCB = numeroCB;
    }

    public Date getDateExpirationCB() {
        return dateExpirationCB;
    }

    public void setDateExpirationCB(Date dateExpirationCB) {
        this.dateExpirationCB = dateExpirationCB;
    }

    // Login
    private String nom;
    private  String prenom;
    private boolean authentifie=false;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String doLogin(){
        this.client = ServiceCart.getSingleton().findOrCreate(this.nom, this.prenom);
        this.authentifie=true;

        this.panier = new ArrayList<>();
        this.infoCB = new InfoCB();
        return "showItems";
    }

    public String doHome(){
        this.client=null;
        this.authentifie=false;
        this.panier=null;

        return "index";
    }

    // ------------------------------------------ ShowItems
    private Item itemSelectionne;
    public List<Item> getEnsItems(){
        return ServiceItem.getSingleton().findAll();
    }

    // ajout d'un article dans le panier
    public String doSelectItem(Item item){
        this.itemSelectionne = item;
        this.panier.add(new Achat(item.getId(), item.getTitre(), item.getPrix(), 1));
        return "showPanier";
    }

    // ---------------------------------------------- Show Panier
    Integer nouvQte;
    Achat achatSelectionne;

    public List<Achat> getPanier(){
        return panier;
    }

    public Double getTotalPanier(){
        Double total = 0.0d;
        for(Achat achat : panier){
            total= total+achat.getPrix()*achat.getQuantite();
        }
        return total;
    }

    public void updateQte(ValueChangeEvent event){
        nouvQte = (Integer)event.getNewValue();
    }

    // retrait d'un article du panier
    public String doSupprimerAchat(Achat achat){
        this.achatSelectionne = achat;
            this.panier.remove(achat);

        if(this.panier.isEmpty()){
            return "showItems";
        }
        return "showPanier";
    }


    // modification d'un article du panier
    public String doModifierAchat(Achat achat){
        this.achatSelectionne = achat;
        achatSelectionne.setQuantite(nouvQte);
        this.panier.remove(achat);
        this.panier.add(achatSelectionne);
        return null;
    }

    public String doListeItems(){
        return "showItems";
    }

    //-------------------------------------------- Validation
    public String doValidatation()  {
        this.infoCB= new InfoCB(numeroCB, dateExpirationCB);
        try {
            ServiceCart.getSingleton().validerPanier(client, panier, infoCB);
        } catch(CardExpiredException ex){

        }
        this.infoCB=null;
        this.client=null;
        this.panier=null;

        this.numeroCB=null;
        this.dateExpirationCB=null;

/*        FacesContext ctx = FacesContext.getCurrentInstance();
        ctx.getExternalContext().invalidateSession();*/

        return "logout";
    }
}
