package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery( name="Client.findAll",
                query="SELECT cl FROM Client cl ORDER BY cl.nom, cl.prenom"
        ),
        @NamedQuery( name="Client.findByNomAndPrenom",
                query="SELECT cl FROM Client cl WHERE UPPER(cl.nom)    = UPPER(:nom) and " +
                                                 "    UPPER(cl.prenom) = UPPER(:prenom)"
        )
})
public class Client implements Serializable {
    private int id;
    private String nom;
    private String prenom;
    private List<Commande> commandes = new ArrayList<>();
    private List<Payment> payments = new ArrayList<>();

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @OneToMany(mappedBy = "client")
    public List<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(List<Commande> commandes) {
        this.commandes = commandes;
    }

    @OneToMany(mappedBy = "client")
    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id &&
                Objects.equals(nom, client.nom) &&
                Objects.equals(prenom, client.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nom, prenom);
    }

    public Client() {
    }

    public Client(String nom, String prenom){
        this.setNom(nom);
        this.setPrenom(prenom);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                '}';
    }
}
