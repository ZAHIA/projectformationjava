package client;

import modele.Client;
import modele.Item;
import service.ServiceCart;
import service.ServiceItem;
import util.Achat;
import util.CardExpiredException;
import util.InfoCB;

import java.util.*;

public class Main {
    public static void main(String[] args) {

            for(Item item : ServiceItem.getSingleton().findAll()){
                System.out.println(item);
            };

/*
            // Java 8 --> prog. fonctionnelle
            ServiceItem.getSingleton().findAll().forEach(System.out::println);

           Client client = ServiceCart.getSingleton().findOrCreate("afeux", "pierre");
//
//        System.out.println(client);

        ServiceCart.getSingleton().findAllClient().forEach(System.out::println);

        List<Achat> panier = new ArrayList<>();
        panier.add(new Achat(1, 3));
        panier.add(new Achat(2,5));

        System.out.println(ServiceCart.getSingleton().valeurDuPanier(panier));
        System.out.println(ServiceCart.getSingleton().contenuDuPanierValue(panier));

        String creditCardNumber = "1234-5678-9012-3456";
        Date expirationDate = new GregorianCalendar(
                            2001,
                                Calendar.JULY,
                                1)
                .getTime();

        InfoCB infoCB = new InfoCB(creditCardNumber, expirationDate);
        try {
            ServiceCart.getSingleton().validerPanier(client, panier, infoCB);
        }catch(CardExpiredException ex){
            System.err.println(ex.getMessage());
        }
*/
    }
}
