package modele;

public class Main {
    public static void main(String[] args) {
        TintinMania t1 = new TintinMania();
        t1.chargerAlbums();

        System.out.println("---------------------------- P A R  T I T R E -----------------------------------");
        t1.listeAlbumsParTitre();
        System.out.println("----------------- P A R  T I T R E  D E C R O I S S A N T -----------------------");
        t1.listeAlbumsParTitreDec();
        System.out.println("---------------------------- P A R  A N N E E -----------------------------------");
        t1.listeAlbumsParAnnee();
        System.out.println("---------------- P A R  A N N E E  D E C R O I S S A N T E ----------------------");
        t1.listeAlbumsParAnneeDec();

    }

}
