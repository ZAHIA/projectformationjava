package modele;

import java.util.Objects;

public class Album implements Comparable<Album>{
    //---------------- attributs d'instance -----------------------
    public String titre;
    public Integer annee;

    //--------------------- setter ---------------------------------
    public void setTitre(String titre) {
        this.titre = (titre==null?"INCONNU":titre.toUpperCase());
    }

    public void setAnnee(Integer annee) {
        this.annee = (annee==null?1985:annee);
    }

    //------------------- getter -----------------------------------
    public String getTitre() {
        return titre;
    }

    public Integer getAnnee() {
        return annee;
    }

    // ----------------------- toString -----------------------------
    @Override
    public String toString() {
        return "Album :" +
                "titre = '" + titre + '\'' +
                ", annee =" + annee +
                "";
    }

    //----------------------- constructeur --------------------------
    public Album(String titre, Integer annee) {
        this.setTitre(titre);
        this.setAnnee(annee);
    }

    public Album() {
        this.setTitre(null);
        this.setAnnee(null);
    }

    //---------------------- equals -----------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Album)) return false;
        Album album = (Album) o;
        return titre.equals(album.titre) &&
                annee.equals(album.annee);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titre, annee);
    }

    //------------------- Comparable -----------------------
    @Override
    public int compareTo(Album autre) {
        return titre.compareTo(autre.titre);
    }
}
