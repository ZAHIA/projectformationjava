package client;

public class Main {
    public static void main(String[] args) {
        Client client = new Client();
        client.ajoutPersonne("Lefebvre", "Michael", 1200.0);
        client.ajoutPersonne("Bouchez", "Marc", 1300.0);
        client.ajoutPersonne("Bibi", "thomas", 700.0);
        client.ajoutPersonne("Moustache", "Jeremy", 1400.0);

        System.out.println("\n----------------Patronyme croissant-----------------------\n");

        client.afficherPatronymeCroissant();

        System.out.println("\n---------------- Patronyme decroissant-----------------------\n");

        client.afficherPatronymeDeroissant();

        System.out.println("\n-----------------Salaire croissant----------------------\n");

        client.afficherSalaireCroissant();

        System.out.println("\n----------------Salaire decroissant-----------------------\n");

        client.afficherSalaireDeroissant();

        System.out.println("\n---------------------------------------");




    }
}
