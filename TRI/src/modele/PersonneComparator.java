package modele;

import java.util.Comparator;

public class PersonneComparator implements Comparator<Personne> {
    @Override
    public int compare(Personne un, Personne deux) {
        int aux = un.getSalaire().compareTo((deux.getSalaire()));

        if (aux!=0)
            return aux;
        else
            return un.getPatronyme().compareTo(deux.getPatronyme());
    }
}
