package modele;

import java.util.Set;
import java.util.TreeSet;

public class Personne implements Comparable<Personne> {
    //attributs d'instance
    private int matricule;
    public Double salaire;
    private Patronyme patronyme;

    //attributs de classe
    private static int dernierMatricule = 0;

    //setter
    public void setSalaire(Double salaire) {
        this.salaire = (salaire==null?1000:(salaire<1000?1000:salaire));
    }

    public void affecterMatricule() {
        Personne.dernierMatricule++;
        this.matricule = Personne.dernierMatricule;
    }

    //getter
    public int getMatricule() {
        return matricule;
    }

    public Patronyme getPatronyme() {
        return patronyme;
    }

    public Double getSalaire() {
        return salaire;
    }

    //constructeur


    public Personne(String nom, String prenom,Double salaire) {
        this.patronyme = new Patronyme(nom,prenom);
        this.setSalaire(salaire);
        this.affecterMatricule();
    }

    public Personne(){
        this.patronyme = new Patronyme(null,null);
        this.setSalaire(null);
        this.affecterMatricule();
    }

    //to string


    @Override
    public String toString() {
        return "Personne{" +
                "matricule=" + matricule +
                ", salaire=" + salaire +
                ", patronyme=" + patronyme +
                '}';
    }

    //compareto
    @Override
    public int compareTo(Personne autre) {
        return patronyme.compareTo(autre.patronyme);
    }

    public static void main(String[] args) {
        Personne p1 = new Personne();
        Personne p2 = new Personne("moche", "mechant", 1200.0);
        Personne p3 = new Personne("moche", "facher", 1300.0);

        Set<Personne> ensPersonne = new TreeSet<>();
        ensPersonne.add(p1);
        ensPersonne.add(p2);
        ensPersonne.add(p3);

        System.out.println(ensPersonne);

        System.out.println("-----------------------------------------------");

        Set<Personne> ensPersonne2 = new TreeSet<>(new PersonneComparator());
        ensPersonne2.add(p1);
        ensPersonne2.add(p2);
        ensPersonne2.add(p3);

        System.out.println(ensPersonne2);
    }
}
