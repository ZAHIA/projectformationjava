package dto;

import java.io.Serializable;

public class ClientDTO implements Serializable {
    private String nom;
    private String pass;

    //GETTER SETTER
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPass() {
        return pass;
    }

    public void setPrenom(String pass) {
        this.pass = pass;
    }

    //Constructeur
    public ClientDTO(String nom, String pass) {
        this.nom = nom;
        this.pass = pass;
    }

    //to string
    @Override
    public String toString() {
        return "ClientDTO{" +
                "nom='" + nom + '\'' +
                ", prenom='" + pass + '\'' +
                '}';
    }
}
