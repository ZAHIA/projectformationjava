package dto;
import dto.ProduitDTO;
import java.io.Serializable;
import java.util.*;


public class Catalogue implements Serializable {
    private Map<String, ProduitDTO> produits= new HashMap<>();

    synchronized public Map<String, ProduitDTO> getProduits(){
        //renvoie une copie en lecture seule
        return Collections.unmodifiableMap(produits);
    }
    synchronized public ProduitDTO getProduit(String nProd){
        return produits.get(nProd);
    }

    synchronized public void setProduit(String nProd, ProduitDTO produit){
        produits.put(nProd, produit);
    }

    public Catalogue(Map<String, ProduitDTO> produits){
        this.produits=produits;
    }
}
