package dao;

import dto.*;

import java.sql.*;
import java.util.*;

public enum EcommerceDAO{
    INSTANCE;

        //IDENTIFICATION BASE DE DONNEES
        private final static String URL = "jdbc:mysql://localhost:3306/ecommerce";
        private final static String USER = "michael";
        private final static String PW = "mdppopmichael";

        //SINGLETON : DECLARATION
        private Connection connection = null;

        private EcommerceDAO(){
            try{
                connection = DriverManager.getConnection(URL, USER, PW);

            }catch (Exception e){
                System.out.println(e);;
            }
        }


        private static String SQLFindAllItems =
    "SELECT nproduit, descriptif, prix, disponible, qteEnStock FROM ECOMM_PRODUIT";

    private static String SQLFindClientByNom =
                "SELECT motDePasse from ECOMM_CLIENT WHERE nom = ? ";

    private static String SQLInsertStatementStr =
            "INSERT INTO ECOMM_COMMANDE " +
                    " VALUES(?, current_timestamp, ?)";

    private static String SQLDetailInsertStatementStr =
            "INSERT INTO ECOMM_DETAIL_CDE " +
                    " VALUES(?, ?, ?)";

    private static String SQLUpdateProduit =
            "UPDATE ECOMM_PRODUIT " +
                    " SET QTEENSTOCK = QTEENSTOCK-1 " +
                    " WHERE NPRODUIT = ?";

    public Catalogue getCatalogue(){
        Map<String, ProduitDTO> items = new HashMap<>();
        try{
            PreparedStatement selectStatement =
                    connection.prepareStatement(SQLFindAllItems);

            ResultSet rs = selectStatement.executeQuery();
            while (rs.next()){
                String nproduit = (rs.getString("nproduit")).trim();
                String descript = rs.getString("descriptif");
                float prix = rs.getFloat("prix");
                boolean disponible = (rs.getString("disponible")).equals("vrai");
                int stock = rs.getInt("qteEnStock");

                items.put(nproduit, new ProduitDTO(nproduit, descript, prix, disponible, stock));
            }

        }catch (SQLException ex){
            ex.printStackTrace();
        }
        return new Catalogue(items);
    }

    public ClientDTO findClientsByNom(String nom) throws SQLException {
        String motDePasse = null;

        PreparedStatement selectStatement = connection.prepareStatement(SQLFindClientByNom);
        selectStatement.setString(1, nom);
        ResultSet rs = selectStatement.executeQuery();
        if (rs.next()){
            motDePasse = rs.getString("motDePasse");
            return new ClientDTO(nom, motDePasse);
        }
        return null;
    }
    //le cache catalogue doit etre mis a jour donc il doit etre passé en parametre
    //les tables commande, detail_cmde
    public void createPanier(Panier panier, Catalogue catalogue) throws SQLException{
        connection.setAutoCommit(false);

        //mise en place d'une transaction
        PreparedStatement insertStatement = connection.prepareStatement(SQLInsertStatementStr);
        PreparedStatement detailInsertStatement = connection.prepareStatement(SQLDetailInsertStatementStr);
        PreparedStatement updateProduitStatement = connection.prepareStatement(SQLUpdateProduit);

        //enregistrement de l'entete de la commande
        insertStatement.setLong(1,panier.getnCommande());
        insertStatement.setString(2, panier.getClient().getNom());

        int nb = insertStatement.executeUpdate();

        //renregistrement des details de la commande et mise a jour stock
        for (String refItem : panier.getItemIds()){
            //creation d'un detail
            detailInsertStatement.setLong(1, panier.getnCommande());
            detailInsertStatement.setLong(2, 1);
            detailInsertStatement.setString(3, refItem);
            nb = detailInsertStatement.executeUpdate();

            updateProduitStatement.setString(1, refItem);
            nb = updateProduitStatement.executeUpdate();

            ProduitDTO produit = catalogue.getProduit(refItem);
            produit.setStock(produit.getStock()-1);
            catalogue.setProduit(refItem, produit);


        }
        connection.commit();
    }

    public static void main(String[] args) {


    }
}
