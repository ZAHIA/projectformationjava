<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 26/02/2020
  Time: 11:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Details</title>
</head>
<body>
<form method="post" action="process?action=validateCommande">
    <table align="center">
        <thead>
        <th>id</th>
        <th>descriptif</th>
        <th>prix</th>
        <th>disponible</th>
        <th>stock</th>
        </thead>
        <tbody>
        <c:forEach items="${anOrder.itemIds}" var="cle">
        <tr>
            <td> <input type="checkbox" checked="checked" name="itemId" value="${catalogue.produits[cle].nProd}"/>
            </td>
            <td>${catalogue.produits[cle].descript}</td>
            <td>${catalogue.produits[cle].prix}</td>
            <td>${catalogue.produits[cle].dispo}</td>
            <td>${catalogue.produits[cle].stock}</td>
        </tr>
        </c:forEach>
        <tfoot>
        <tr>
            <td colspan="5">
                <input type="submit" value="envoie">
            </td>
        </tr>
        </tfoot>
        </tbody>
    </table>

</form>
<%@include file="footer.jsp"%>
</body>
</html>
