package modele;

import com.mysql.jdbc.jdbc2.optional.MysqlConnectionPoolDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class MyConnection {
    private final static String SERVER_NAME = "localhost";
    private final static int PORT = 3306;
    private final static String DATABASE_NAME = "employes";
    private final static String USER = "michael";
    private final static String PW = "mdppopmichael";
    private static Connection connect;

    private MyConnection(){
        try{
            MysqlConnectionPoolDataSource dataSource = new MysqlConnectionPoolDataSource();

            dataSource.setUser(USER);
            dataSource.setPassword(PW);
            dataSource.setServerName(SERVER_NAME);
            dataSource.setPort(PORT);
            dataSource.setDatabaseName(DATABASE_NAME);

            connect = dataSource.getConnection();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    public static Connection getInstance(){
        if(connect == null){
            new MyConnection();
        }
        return connect;
    }

}
