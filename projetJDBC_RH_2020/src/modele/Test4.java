package modele;

import javax.swing.*;
import java.lang.reflect.Array;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Test4 {
    private final static String SQLselect =
            "SELECT ename, sal, comm, deptno FROM employee "
                    +   "WHERE deptno = ? "
                    +   "ORDER BY ename";
    private final static String SQLupdate =
            "UPDATE employee "
                    +   " SET sal = sal*(1.0+?/100.0)"
                    +   "WHERE deptno = ?";

    public static void main(String[] args) {
        int [][] augmentation = {{20,5},{30,8},{10,15}};


        Connection conn = null;
        Connection conn2 = null;
        PreparedStatement pstmt = null;
        PreparedStatement pstmtu = null;

        try {

            conn = MyConnection.getInstance();
            conn2 = MyConnection.getInstance();
            conn.setAutoCommit(false);
            pstmt = conn.prepareStatement(SQLselect);
            pstmtu = conn2.prepareStatement(SQLupdate);

            //avant l'augmentation
            for (int i = 0; i < augmentation.length; i++) {
                pstmt.setInt(1, augmentation[i][0]);
                ResultSet rset = pstmt.executeQuery();
                System.out.println("*** Dept " + augmentation[i][0] + " avant augmentation");

                //recuperer les donnees
                while (rset.next()){
                    System.out.println(rset.getString(1) + " gagne " + rset.getFloat(2));
                }
            }

            //augmentation
            for (int i = 0 ; i<augmentation.length;i++){
                pstmtu.setInt(1,augmentation[i][1]);
                pstmtu.setInt(2, augmentation[i][0]);
                int rset2 = pstmtu.executeUpdate();
                conn.commit();
            }

            //apres
            for (int i = 0; i < augmentation.length; i++) {
                pstmt.setInt(1, augmentation[i][0]);
                ResultSet rset = pstmt.executeQuery();
                System.out.println("*** Dept " + augmentation[i][0] + " apres augmentation");

                //recuperer les donnees
                while (rset.next()){
                    System.out.println(rset.getString(1) + " gagne " + rset.getFloat(2));
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }




}
