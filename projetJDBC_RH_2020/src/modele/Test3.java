package modele;

import javax.sql.rowset.serial.SerialStruct;
import java.sql.*;

public class Test3 {
    private final static String SQLselectEmployeesOfDept =
                "SELECT ename, sal, comm, deptno FROM employee "
            +   "WHERE deptno = ? "
            +   "ORDER BY ename";

    private final static String SQLupdateEmployeesOfDept =
                "UPDATE employee "
            +   " SET sal = sal*(1.0+?/100.0)"
            +   "WHERE deptno = ?";

    private final static String SQLMassesalaire =
                " SELECT emp.deptno "
            +   " FROM ( "
            +   " select deptno, SUM(sal)total "
            +   " from employee "
            +   " GROUP BY deptno "
            +   " ORDER BY total desc"
            +   " )emp "
            +   "LIMIT 1";

    private final static String SQLcomplet =
                "SELECT ename, sal, comm, deptno "
            +   "FROM employee "
            +   "WHERE (DEPTNO LIKE (SELECT emp.deptno "
            +   "FROM ( "
            +   " select deptno, SUM(sal)total "
            +   " from employee "
            +   " GROUP BY deptno "
            +   " ORDER BY total desc "
            +   " )emp "
            +   " LIMIT 1))";

    public static void main(String[] args) throws SQLException, ClassNotFoundException,java.io.IOException{
        //declaration des  variable pour la connection a la base de données
        String DRIVER = "com.mysql.jdbc.Driver";
        String URL = "jdbc:mysql://localhost:3306/employes";
        String USER = "michael";
        String PW = "mdppopmichael";

        Connection conn = null;
        PreparedStatement pstmSelect = null;
        PreparedStatement pstmSelectMasse = null;
        PreparedStatement pstmSelectMasse2 = null;
        PreparedStatement pstmUpdate = null;

        ////chargement du pilote (inutile depuis JDK 8 si il est dans la librairie
        Class.forName(DRIVER);

        try{
            //connection a la base de données
            //conn = DriverManager.getConnection(URL, USER, PW);
            conn = MyConnection.getInstance();

            pstmSelect = conn.prepareStatement(SQLselectEmployeesOfDept);
            pstmSelect.setInt(1,10);
            ResultSet rs = pstmSelect.executeQuery();
            while(rs.next()  ) {
                String nom = rs.getString("ename");
                Double salaire = rs.getDouble("sal");
                Double comm = rs.getDouble("comm");
                String commentaire = (rs.wasNull()
                                            ? " pas de commision "
                                            : " commission de " + comm);
                System.out.println(nom + " gagne " + salaire + commentaire);
            }
            //Mise a jour

            conn.setAutoCommit(false);

            pstmUpdate= conn.prepareStatement(SQLupdateEmployeesOfDept);
            pstmUpdate.setDouble(1,10);
            pstmUpdate.setInt(2,10);

            int nb = pstmUpdate.executeUpdate();
            conn.commit();
            System.out.println("-------------------------------");

            pstmSelect.setInt(1,10);
            rs = pstmSelect.executeQuery();
            while(rs.next()  ) {
                String nom = rs.getString("ename");
                Double salaire = rs.getDouble("sal");
                Double comm = rs.getDouble("comm");
                String commentaire = (rs.wasNull()
                        ? " pas de commision "
                        : " commission de " + comm);
                System.out.println(nom + " gagne " + salaire + commentaire);
            }
            System.out.println("--------------------------------");

            //------liste employee des departemetn masse salariale max ----------------------
            pstmSelectMasse = conn.prepareStatement(SQLMassesalaire);

            ResultSet rs2 = pstmSelectMasse.executeQuery();

            if(rs2.next()) {
                int departement = rs2.getInt("emp.deptno");
                System.out.println("departement  : " + departement);

                pstmSelect.setInt(1, departement);

                rs = pstmSelect.executeQuery();
                while(rs.next()  ) {
                    String nom = rs.getString("ename");
                    Double salaire = rs.getDouble("sal");
                    Double comm = rs.getDouble("comm");
                    String commentaire = (rs.wasNull()
                            ? " pas de commision "
                            : " commission de " + comm);
                    System.out.println(nom + " gagne " + salaire + commentaire);
                }
            }

            System.out.println("-------------- C O M P L E T ---------------");
            pstmSelectMasse2 = conn.prepareStatement(SQLcomplet);

            ResultSet rs3 = pstmSelectMasse2.executeQuery();
            boolean test = false;
                while(rs3.next()  ) {
                    if(test == false ) {
                        System.out.println("Le departement est : " + rs3.getInt("deptno"));
                        test = true;
                    }
                    String nom = rs3.getString("ename");
                    Double salaire = rs3.getDouble("sal");
                    Double comm = rs3.getDouble("comm");
                    String commentaire = (rs3.wasNull()
                            ? " pas de commision "
                            : " commission de " + comm);
                    System.out.println(nom + " gagne " + salaire + commentaire);
                }


        } finally {
            if (pstmSelect != null){
                pstmSelect.close();
            }
            //spprimer pour ne pas fermer la connection de my connection
            /*if (conn != null){
                conn.close();
            }*/
        }

    }


}
