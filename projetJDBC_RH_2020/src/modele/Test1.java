package modele;

import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.*;

public class Test1 {
    public void test1()throws SQLException, ClassNotFoundException,java.io.IOException{
        //declaration des  variable pour la connection a la base de données
        String DRIVER = "com.mysql.jdbc.Driver";
        String URL = "jdbc:mysql://localhost:3306/employes";
        String USER = "michael";
        String PW = "mdppopmichael";

        Connection conn = null;
        Statement stmt = null;

        try{
            ////chargement du pilote (inutile depuis JDK 6 si il est dans la librairie
            Class.forName(DRIVER);

            //connection a la base de données
            //conn = DriverManager.getConnection(URL, USER, PW); // en utilisant une connection par classe
            conn = MyConnection.getInstance(); // en utilisant une classe de connection
            //conn = MyConnection.getInstance();
            stmt = conn.createStatement();
            ResultSet resultat = stmt.executeQuery(
                    "SELECT dname, count(*) as nbre "
                    +   " FROM employee "
                    +   " inner join DEPT "
                    +   " using(deptno) "
                    +   " group by dname "
                    +   " order by dname");
            while( resultat.next()  ) {
                System.out.println(resultat.getString("dname") + " a " + resultat.getInt("nbre") + " employes");
            }
        } finally {
            if (stmt != null){
                stmt.close();
            }
            //enlever pour ne pas fermer la connection de  la classe myconnection
           /* if (conn != null){
                conn.close();
            }*/
        }

    }


    public static void main(String[] args) {
        Test1 ts = new Test1();
        try {
            ts.test1();
        }catch (ClassNotFoundException | SQLException | IOException e){
            //TODO Auto generated catch block
            e.printStackTrace();
        }
    }
}
