import java.sql.*;

public class Test2 {

    private static String SQLLajoutEmploye =
            "INSERT INTO employee (EMPNO, ENAME, SAL, DEPTNO)"
                    + " VALUES(6789, 'BELIVIER', 500, 10)";
    private static String SQLLajoutEmploye2 =
            "INSERT INTO employee (EMPNO, ENAME, SAL, DEPTNO)"
                    + " VALUES(6790, 'TOLIVER', 500, 10)";
    private static String SQLLajoutEmploye3 =
            "INSERT INTO employee (EMPNO, ENAME, SAL, DEPTNO)"
                    + " VALUES(6791, 'MARCOLIV', 500, 10)";

    private static String SQLmodificationEmploye =
                "UPDATE employee "
            +   " SET SAL = SAL * 1.1 "
            +   " WHERE SAL IS NOT NULL "
            +   " ORDER BY SAL "
            +   " LIMIT 3 "
    ;


    public static void main(String[] args) throws SQLException, ClassNotFoundException, java.io.IOException {

//declaration des  variable pour la connection a la base de données
        String DRIVER = "com.mysql.jdbc.Driver";
        String URL = "jdbc:mysql://localhost:3306/employes";
        String USER = "michael";
        String PW = "mdppopmichael";

        Connection conn = null;
        Statement stmt = null;
        ////chargement du pilote (inutile depuis JDK 8 si il est dans la librairie
        Class.forName(DRIVER);

        try {
            //connection a la base de données
            conn = DriverManager.getConnection(URL, USER, PW);
            conn.setAutoCommit(false);
            //conn = MyConnection.getInstance();
            stmt = conn.createStatement();

            int nbLignes = stmt.executeUpdate(SQLLajoutEmploye);
            int nbLignes2 = stmt.executeUpdate(SQLLajoutEmploye2);
            int nbLignes3 = stmt.executeUpdate(SQLLajoutEmploye3);

            nbLignes = stmt.executeUpdate(SQLmodificationEmploye);

            conn.commit();
            System.out.println(nbLignes);
            System.out.println(nbLignes2);
            System.out.println(nbLignes3);

        }catch (SQLException ex){
            ex.printStackTrace();
            conn.rollback();
        }
        finally {
            if (stmt != null) {
                stmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }


    }
}
