package modele;

import javax.annotation.Generated;
import javax.persistence.*;
import java.io.Serializable;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Job.findAll",
                query = "SELECT j FROM Job j ORDER BY j.jobName"),
        @NamedQuery(name = "Job.findByJobName",
                query = "SELECT j FROM Job j " +
                        "WHERE upper(j.jobName)= upper(:jobName) ")
})
public class Job implements Serializable {
    private int jobId;
    private String jobName;
    private Integer score;
    private Double setupcost;
    private Set<Gangster> gangsters = new HashSet<>();


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "JOB_ID")
    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "JOB_NAME")
    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Basic
    @Column(name = "SCORE")
    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    @Basic
    @Column(name = "SETUPCOST")
    public Double getSetupcost() {
        return setupcost;
    }

    public void setSetupcost(Double setupcost) {
        this.setupcost = setupcost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Job)) return false;
        Job job = (Job) o;
        return jobName.equals(job.jobName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobName);
    }

    @ManyToMany(mappedBy = "jobs")

    public Set<Gangster> getGangsters() {
        return gangsters;
    }

    public void setGangsters(Set<Gangster> gangsters) {
        this.gangsters = gangsters;
    }

    @Override
    public String toString() {
        return "Job{" +
                "jobId=" + jobId +
                ", jobName='" + jobName + '\'' +
                ", score=" + score +
                ", setupcost=" + setupcost +
                ", gangsters=" + gangsters +
                '}';
    }

    public Job() {
    }

    public Job(String jobName, Integer score, Double setupcost) {
        this.setJobName(jobName);
        this.setScore(score);
        this.setSetupcost(setupcost);
    }
}
