package modele;

import com.sun.org.apache.xpath.internal.operations.Or;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.*;

@Entity
@NamedQueries({
        @NamedQuery(name = "Organisation.findAll",
                query = "SELECT o FROM Organisation o"),
        @NamedQuery(name = "Organisation.findByOrgName",
                query = "SELECT o FROM Organisation o WHERE upper(o.orgName) = upper(:orgName)")
})
public class Organisation implements Serializable {
    private String orgName;
    private String description;
    private Set<Gangster> gangsters = new HashSet<>();
    private Gangster boss;

    //constructeur


    public Organisation() {
    }

    public Organisation(String orgName, String description) {
        this.orgName = orgName;
        this.description= description;
    }

    @Id
    @Column(name = "ORG_NAME")
    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    @Basic
    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Organisation)) return false;
        Organisation that = (Organisation) o;
        return orgName.equals(that.orgName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orgName);
    }

    @OneToMany(mappedBy = "organisation")
    public Set<Gangster> getGangsters() {
        return gangsters;
    }

    public void setGangsters(Set<Gangster> gangsters) {
        this.gangsters = gangsters;
    }

    @OneToOne
    @JoinColumn(name = "THEBOSS", referencedColumnName = "GANGSTER_ID")
    public Gangster getBoss() {
        return boss;
    }

    public void setBoss(Gangster newboss) {
        if (newboss==null)return;
        if(!this.gangsters.contains(newboss))return;
        if (this.boss != null){
            if (!this.boss.equals(newboss)){
                this.boss.setOrganisation(null);
            }else {
                return;
            }
        }
        this.boss = newboss;
        newboss.setOrganisationGeree(this);
    }

    @Override
    public String toString() {
        return "Organisation{" +
                "orgName='" + orgName + '\'' +
                ", description='" + description + '\'' +
                ", gangsters=" + gangsters +
                ", boss=" + boss +
                '}';
    }


    public void addGangster(Gangster gangster){
        if (gangster == null) return;

        if (gangster.getOrganisation()==null){
            gangster.setOrganisation(this);
            this.gangsters.add(gangster);
        }else {
            if (gangster.getOrganisation().equals(this)) return;
            gangster.getOrganisation().gangsters.remove(gangster);
            if (gangster.getOrganisation().getBoss()!=null &&
                    gangster.getOrganisation().getBoss().equals(gangster)){
                gangster.getOrganisation().setBoss(null);
            }
            gangster.setOrganisation(this);
            this.gangsters.add(gangster);
        }
    }

    public static void main(String[] args) {


    }
}
