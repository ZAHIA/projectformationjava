package controleur;

import modele.Gangster;
import modele.Organisation;
import service.Service;

import javax.faces.bean.ManagedBean;
import java.util.*;
import java.io.Serializable;

@ManagedBean(name = "orgCtrl")
public class OrgControl implements Serializable {

    //--------------------------------------ajout organisation -----------------------------
    private Organisation nouvOrg = new Organisation();

    public Organisation getNouvOrg() {
        return nouvOrg;
    }

    public void setNouvOrg(Organisation nouvOrg) {
        this.nouvOrg = nouvOrg;
    }

    public String doAjoutOrganisation(){
        Service.getSingleton().createOrganisation(nouvOrg);
        return "index";
    }
///---------------------------------retrait organisation ---------------------------------------
    public String doRetraitOrganisation(){
        Service.getSingleton().removeOrganisation(orgChoisi);
        return "index";
    }

    private String orgChoisi;

    public String getOrgChoisi() {
        return orgChoisi;
    }

    public void setOrgChoisi(String orgChoisi) {
        this.orgChoisi = orgChoisi;
    }

    public List<Organisation> getOrgs(){
        return Service.getSingleton().findAllOrganisation();
    }

//----------------------------------retour a home ----------------------------
    public String doHome(){
        return "index";
    }
    public String doOrgSupp(){
        return "orgSupp";
    }

    public String doOrgAjout(){
        return "orgAjout";
    }

    public String doOrgGangsters(){
        return "orgGangsters";
    }

    //------------afficher les gangster de l'organisation ------------
    public List<Organisation> getEnsOrgs(){

        return Service.getSingleton().findAllOrganisation();
    }

    private String orgNameChoisi;

    public String getOrgNameChoisi() {
        return orgNameChoisi;
    }

    public void setOrgNameChoisi(String orgNameChoisi) {
        this.orgNameChoisi = orgNameChoisi;
    }

    private List<Gangster> gangster = null;

    public List<Gangster> getGangster(){
        return Service.getSingleton().findGangsterOfOrg(orgNameChoisi);
    }

    //--------------------------


}
