package controleur;

import modele.Gangster;
import modele.Organisation;
import service.Service;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "gangCtrl")
@ViewScoped
public class GangControl implements Serializable {

    //--------------------------------------ajout gangster -----------------------------
    private Gangster nouvGang = new Gangster();

    public Gangster getNouvGang() {
        return nouvGang;
    }

    public void setNouvGang(Gangster nouvGang) {
        this.nouvGang = nouvGang;
    }

    public String doAjoutGangster(){
        Service.getSingleton().createGangster(nouvGang);
        return "index";
    }
///---------------------------------suppression gangster ---------------------------------------
    public String doRetraitGangster(){
        Service.getSingleton().removeGangster(gangChoisi);
        return "index";
    }

    public String gangChoisi;

    public String getGangChoisi() {
        return gangChoisi;
    }

    public void setGangChoisi(String gangChoisi) {
        this.gangChoisi = gangChoisi;
    }

    public List<Gangster> getGangs(){
        return Service.getSingleton().findAllGangster();
    }

//----------------------------------retour a home ----------------------------
    public String doHome(){
        return "index";
    }
    public String doGangSupp(){
        return "gangSupp";
    }

    public String doGangAjout(){
        return "gangAjout";
    }

    public String doGangBoss(){
        return "gangBoss";
    }
    public String doGangAffect(){
        return "gangAffectation";
    }

    public String doOrgGangsters(){
        return "orgGangsters";
    }

    //--------------------gangster affectation a une organisation----------------------------

        //----------liste gangster
    public String gangChoisiAffectation;

    public String getGangChoisiAffectation() {
        return gangChoisiAffectation;
    }

    public void setGangChoisiAffectation(String gangChoisiAffectation) {
        this.gangChoisiAffectation = gangChoisiAffectation;
    }

    public List<Gangster> getGangsAffect() {
        return Service.getSingleton().findAllGangster();
    }
    public List<Gangster> getGangsAffectOfOrg() {
        System.out.println(orgChoisiAffectationBoss);
        if (orgChoisiAffectationBoss==null) return null;
        return Service.getSingleton().findGangsterOfOrg(orgChoisiAffectationBoss);
    }

        //------liste organisation

    private String orgChoisiAffectation;

    public String getOrgChoisiAffectation() {
        return orgChoisiAffectation;
    }

    public void setOrgChoisiAffectation(String orgChoisiAffectation) {
        this.orgChoisiAffectation = orgChoisiAffectation;
    }
    private String orgChoisiAffectationBoss;

    public String getOrgChoisiAffectationBoss() {
        return orgChoisiAffectationBoss;
    }

    public void setOrgChoisiAffectationBoss(String orgChoisiAffectationBoss) {
        this.orgChoisiAffectationBoss = orgChoisiAffectationBoss;
    }

    public List<Organisation> getOrgsAffect(){
        return Service.getSingleton().findAllOrganisation();
    }

        //doaffectation
    public String doAffectationGangster(){
        Service.getSingleton().addGangsterToOrganisation(gangChoisiAffectation,orgChoisiAffectation);
        this.orgChoisiAffectation = null;
        this.gangChoisiAffectation = null;
        return "index";
    }
    public String doAffectationBoss(){
        Service.getSingleton().addTheBossToORganisation(gangChoisiAffectation,orgChoisiAffectationBoss);
        this.gangChoisiAffectation = null;
        this.orgChoisiAffectationBoss = null;
        return "index";
    }
    public String doDesaffectationGangster(){
        Service.getSingleton().removeGangsterFromOrganisation(gangChoisiAffectation);
        return "index";
    }

    //------------afficher les gangster de l'organisation ------------
    public List<Organisation> getEnsOrgs(){

        return Service.getSingleton().findAllOrganisation();
    }

    private String orgNameChoisi;

    public String getOrgNameChoisi() {
        return orgNameChoisi;
    }

    public void setOrgNameChoisi(String orgNameChoisi) {
        this.orgNameChoisi = orgNameChoisi;
    }

    private List<Gangster> gangster = null;

    public List<Gangster> getGangster(){
        return Service.getSingleton().findGangsterOfOrg(orgNameChoisi);
    }

    //--------------------------


}
