package controleur;


import modele.Job;
import service.Service;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import java.util.*;

@ManagedBean(name = "jobCtrl")
@ViewScoped
public class JobControl {
    //---------------------------------Ajout de job------------------------------------
    private Job nouvJob = new Job();

    public Job getNouvJob() {
        return nouvJob;
    }

    public void setNouvJob(Job nouvJob) {
        this.nouvJob = nouvJob;
    }

    public String doAjoutJob(){
        Service.getSingleton().createJob(nouvJob);
        return "index";
    }

///-----------------------------------update job
    public List<Job> jobs=null;
    public List<Job> getJobs(){
        return Service.getSingleton().findAllJob();
    }
    public Job jobAModifier = null;

    public Job getJobAModifier(){
        jobAModifier = Service.getSingleton().findJobById(jobChoisi);
        return jobAModifier;
    }


    public int jobChoisi;

    public int getJobChoisi() {
        return jobChoisi;
    }

    public void setJobChoisi(int jobChoisi) {
        this.jobChoisi = jobChoisi;
        System.out.println(jobChoisi);
    }

    public String doJobUpdate(){
        System.out.println(jobAModifier);
        if (jobAModifier == null)return null;
        Service.getSingleton().updateJob(jobAModifier);
        return "index";
    }
    ///-------------------do page
    public String doJobU(){
        return "jobUpdate";
    }

    public String doJobAjout(){
        return "jobAjout";
    }
}
