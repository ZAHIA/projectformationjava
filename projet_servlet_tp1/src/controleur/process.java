package controleur;

import dao.ClientDAO;
import modele.Client;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/process")
public class process extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    //navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }


    //controleur

    private void processRquest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //recupertaion de l'action
            String action = request.getParameter("action");

            //execution du traitement
            if (action.equals("authenticate")){
                doAuthenticate(request, response);
            }else if (action.equals("login")){
                doLogin(request, response);
            }else if (action.equals("afficherEnrClient")){
                doAfficherFromEnrClient(request, response);
            }else if (action.equals("enrClient")){
                doEnrClient(request, response);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    protected void doLogin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = "pages/login.jsp";
        forward(url, request,response);
    }

    protected void doAfficherFromEnrClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String url = "pages/enregistrement.jsp";
        forward(url, request,response);
    }

    protected void doAuthenticate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //recuperation des données du formaulaire
            String alias = request.getParameter("alias");
            String password = request.getParameter("password");

            //regle metier
            //1 consultaiton de la base de donnée
            Client client = ClientDAO.getSingleton().find(alias, password);
            if (client != null){
                request.setAttribute("client", client);
            }
            //2 cinematique
            String url = (client == null ? "pages/inconnu.jsp" : "pages/connu.jsp");
            forward(url, request,response);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    protected void doEnrClient(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {


        //recuperation des données du formulaire
        String alias = request.getParameter("alias");
        String password = request.getParameter("password");
        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String email = request.getParameter("email");

        // 1 Insertion en base de donnée et ajout d'un objet dans le scope request
        Client client = ClientDAO.getSingleton().create(alias, password, nom,prenom,email);
        if (client != null){
            request.setAttribute("client", client);
        }

        //2 cinematique
        String url = (client == null ? "pages/inconnu.jsp" : "pages//connu.jsp");
        forward(url, request,response);
        }catch (Exception ex){
            System.err.println(ex);
        }
    }
}
