<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 24/02/2020
  Time: 14:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Connection</title>
</head>
<body>
<div id="corp">
    <section id="connect">

        <form method="POST" action="process?action=authenticate">
            <fieldset id="zone">
                <legend>Identifiant de connection</legend>
                <label class="lab" id="lab_mail">Identifiant : </label>
                <input type="text" name="alias" class="input" id="input_mail" placeholder="alias" maxlength="70" required>
                <br>
                <label class="lab" id="lab_pw">Mot de passe : </label>
                <input type="password" name="password" class="input" id="input_pw" placeholder="mot de passe" maxlength="30" required>
            </fieldset>
            <br>

            <input type="submit" class="input" id="input_connect" value="Connection">

        </form>

        <p>
            <span><a href="process?action=afficherEnrClient">Inscription</a></span>
        </p>

    </section>
</div>
</body>
</html>
