<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 24/02/2020
  Time: 14:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Enregistrement</title>
</head>
<body>
<div id="corp">
    <section id="formulaire">

        <form method="POST" action="process?action=enrClient">
            <fieldset id="zone">
                <legend>Informations</legend>
                <label class="lab" id="lab_alias">Alias : </label>
                <input type="text" name="alias" class="input" id="input_alias" placeholder="entrez votre alias" maxlength="70" required>
                <br>
                <label class="lab" id="lab_nom">Nom : </label>
                <input type="text" name="nom" class="input" id="input_nom" placeholder="entrez votre nom" maxlength="70" required>
                <br>
                <label class="lab" id="lab_prenom">Prenom : </label>
                <input type="text" name="prenom" class="input" id="input_prenom" placeholder="votre prénom" maxlength="70" required>
                <br>
                <label class="lab" id="lab_email">Adresse mail : </label>
                <input type="email" name="email" class="input" id="input_mail" placeholder="votre mail" maxlength="70" required>
                <br>
                <label class="lab" id="lab_pw">Mot de passe : </label>
                <input type="password" name="password" class="input" id="input_pw" placeholder="mot de passe" maxlength="30" required>
            </fieldset>
            <br>

            <input type="submit" class="bo_env" id="bp_env" value="Envoyer">

        </form>

        <p>

        </p>

    </section>
</div>
</body>
</html>
