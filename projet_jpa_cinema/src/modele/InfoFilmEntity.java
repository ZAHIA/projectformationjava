package modele;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "Info_film", schema = "cinema", catalog = "")
public class InfoFilmEntity {
    private int nfilm;
    private String titre;
    private String nomRealisateur;
    private String nomActeur;

    @Basic
    @Column(name = "NFILM")
    public int getNfilm() {
        return nfilm;
    }

    public void setNfilm(int nfilm) {
        this.nfilm = nfilm;
    }

    @Basic
    @Column(name = "TITRE")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "NOM_REALISATEUR")
    public String getNomRealisateur() {
        return nomRealisateur;
    }

    public void setNomRealisateur(String nomRealisateur) {
        this.nomRealisateur = nomRealisateur;
    }

    @Basic
    @Column(name = "NOM_ACTEUR")
    public String getNomActeur() {
        return nomActeur;
    }

    public void setNomActeur(String nomActeur) {
        this.nomActeur = nomActeur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InfoFilmEntity that = (InfoFilmEntity) o;
        return nfilm == that.nfilm &&
                Objects.equals(titre, that.titre) &&
                Objects.equals(nomRealisateur, that.nomRealisateur) &&
                Objects.equals(nomActeur, that.nomActeur);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nfilm, titre, nomRealisateur, nomActeur);
    }
}
