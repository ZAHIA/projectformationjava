package modele;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "EMPRUNT", schema = "cinema", catalog = "")
public class EmpruntEntity {
    private int nemprunt;
    private String retour;
    private Date dateEmprunt;
    private ClientEntity clientByNclient;
    private FilmEntity filmByNfilm;

    @Id
    @Column(name = "nemprunt")
    public int getNemprunt() {
        return nemprunt;
    }

    public void setNemprunt(int nemprunt) {
        this.nemprunt = nemprunt;
    }

    @Basic
    @Column(name = "retour")
    public String getRetour() {
        return retour;
    }

    public void setRetour(String retour) {
        this.retour = retour;
    }

    @Basic
    @Column(name = "dateEmprunt")
    public Date getDateEmprunt() {
        return dateEmprunt;
    }

    public void setDateEmprunt(Date dateEmprunt) {
        this.dateEmprunt = dateEmprunt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmpruntEntity that = (EmpruntEntity) o;
        return nemprunt == that.nemprunt &&
                Objects.equals(retour, that.retour) &&
                Objects.equals(dateEmprunt, that.dateEmprunt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nemprunt, retour, dateEmprunt);
    }

    @ManyToOne
    @JoinColumn(name = "nclient", referencedColumnName = "nclient", nullable = false)
    public ClientEntity getClientByNclient() {
        return clientByNclient;
    }

    public void setClientByNclient(ClientEntity clientByNclient) {
        this.clientByNclient = clientByNclient;
    }

    @ManyToOne
    @JoinColumn(name = "nfilm", referencedColumnName = "nfilm", nullable = false)
    public FilmEntity getFilmByNfilm() {
        return filmByNfilm;
    }

    public void setFilmByNfilm(FilmEntity filmByNfilm) {
        this.filmByNfilm = filmByNfilm;
    }
}
