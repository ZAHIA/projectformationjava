package modele;

import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "GENRE", schema = "cinema", catalog = "")
public class GenreEntity {
    private int ngenre;
    private String nature;
    private Collection<FilmEntity> films;

    @Id
    @Column(name = "ngenre")
    public int getNgenre() {
        return ngenre;
    }

    public void setNgenre(int ngenre) {
        this.ngenre = ngenre;
    }

    @Basic
    @Column(name = "nature")
    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenreEntity that = (GenreEntity) o;
        return ngenre == that.ngenre &&
                Objects.equals(nature, that.nature);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ngenre, nature);
    }

    @OneToMany(mappedBy = "genre")
    public Collection<FilmEntity> getFilms() {
        return films;
    }

    public void setFilms(Collection<FilmEntity> films) {
        this.films = films;
    }
}
