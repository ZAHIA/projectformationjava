package controleur;

import modele.Albums;
import modele.Personnages;
import service.Service;

import javax.faces.bean.ManagedBean;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;

@ManagedBean(name = "ctrl")
public class Control {
    private Albums albumChoisi = null;

    public List<Personnages> getPersonnageChoisis(){
        return  Service.getSingleton().findPersonnagesOfAlbum(albumChoisi);
    }

    public Albums getAlbumChoisi() {
        return albumChoisi;
    }

    public List<Albums> getAlbums(){
        return Service.getSingleton().findAllAlbums();
    }

    public List<Albums> getAlbumsByPers(){
        return Service.getSingleton().findAlbumsByPers(idPersonnageChoisis);
    }

    public String doShowPersonnage(Albums albums){
        //to do : recupere les personnage de la page
        this.albumChoisi = albums;
        return "personnages";
    }

    public String doHome(){
        return "index";
    }

    private Integer idPersonnageChoisis;

    public Integer getIdPersonnageChoisis(){
        return idPersonnageChoisis;
    }

    public void setIdPersonnageChoisis(Integer idPersonnageChoisis){
        this.idPersonnageChoisis = idPersonnageChoisis;
    }

    public List<Personnages> getEnsPersonnages(){
        return Service.getSingleton().findAllPersonnage();

    }

    public String doShowAlbumPersonnageChoisis(){
        this.personnageChoisi= Service.getSingleton().findPersonnagesById(this.idPersonnageChoisis);
        return "albumsdupersonnage";
    }

    private Personnages personnageChoisi = null;

    public Personnages getPersonnageChoisi() {
        return personnageChoisi;
    }

    public void setPersonnageChoisi(Personnages personnageChoisi) {
        this.personnageChoisi = personnageChoisi;
    }
///------------------------------------------------------------------------------------------------login

    private Client client = new Client();


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String doAuthentification(){
        if (Service.getSingleton().autentification(client))
            return "index";
        return "null";
    }

    public static void main(String[] args) {
        Personnages p = Service.getSingleton().findPersonnagesById(1);
        System.out.println(p);

    }

}
