package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@Entity
       @NamedQueries({
               @NamedQuery(name = "Albums.findAll", query="SELECT alb FROM Albums alb"),
               @NamedQuery(name = "Albums.findByTitre",
                                   query = "SELECT alb FROM Albums alb WHERE upper(alb.titre)=upper(:titre)"),
               @NamedQuery(name = "Albums.findById",
                       query = "SELECT g FROM Albums g WHERE g.id= :id") ,
               @NamedQuery(name = "Albums.findByPersonnage",
                       query = "SELECT p.albums FROM Apparaits p WHERE p.personnages.id= :personnage")


       })


@Table(name = "albums")



public class Albums implements Serializable {
    private int id;
    private String titre;
    private int annee;
    private Albums albumsBySuivant;
    private Set<Apparaits> apparaits= new HashSet<>();

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(mappedBy ="albums")
    public Set<Apparaits> getApparaits() {
        return apparaits;
    }

    public void setApparaits(Set<Apparaits> apparaits) {
        this.apparaits = apparaits;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "annee")
    public int getAnnee() {
        return annee;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Albums)) return false;
        Albums albums = (Albums) o;
        return titre.equals(albums.titre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titre);
    }

    @ManyToOne
    @JoinColumn(name = "suivant", referencedColumnName = "id")
    public Albums getAlbumsBySuivant() {
        return albumsBySuivant;
    }

    public void setAlbumsBySuivant(Albums albumsBySuivant) {
        this.albumsBySuivant = albumsBySuivant;
    }

    @Override
    public String toString() {
        return "Albums{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", annee=" + annee +
                ", albumsBySuivant=" + albumsBySuivant +
                ", apparaits=" + apparaits +
                '}';
    }
}
