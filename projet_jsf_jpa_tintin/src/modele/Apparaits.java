package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "apparaits")
public class Apparaits implements Serializable {
    private int id;
    private Albums albums;
    private Personnages personnages;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Apparaits apparaits = (Apparaits) o;
        return id == apparaits.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne
    @JoinColumn(name = "album_id", referencedColumnName = "id", nullable = false)
    public Albums getAlbums() {
        return albums;
    }

    public void setAlbums(Albums albums) {
        this.albums = albums;
    }

    @ManyToOne
    @JoinColumn(name = "personnage_id", referencedColumnName = "id", nullable = false)
    public Personnages getPersonnages() {
        return personnages;
    }

    public void setPersonnages(Personnages personnages) {
        this.personnages = personnages;
    }


}
