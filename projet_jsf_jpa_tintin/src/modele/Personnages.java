package modele;

import com.sun.org.apache.xml.internal.serializer.utils.SerializerMessages_sv;
import java.util.*;

import javax.persistence.*;
import javax.sql.rowset.serial.SerialArray;
import java.io.Serializable;
import java.util.Objects;

@Entity

@NamedQueries({
        @NamedQuery(name = "Personnages.findAll", query="SELECT p FROM Personnages p ORDER BY p.nom, p.prenom"),
        @NamedQuery(name = "Personnage.findByNom",
                query = "SELECT p FROM Personnages p WHERE upper(p.nom)=upper(:nom)"),
        @NamedQuery(name = "Personnages.findById",
                query = "SELECT p FROM Personnages p WHERE p.id= :id"),
        @NamedQuery(name = "Personnages.findByAlbum",
                query = "SELECT p.personnages FROM Apparaits p WHERE p.albums= :album")
})
@Table(name = "personnages")
public class Personnages implements Serializable {
    private int id;
    private String nom;
    private String prenom;
    private String profession;
    private String sexe;
    private String genre;
    private Set<Apparaits> apparaits = new HashSet<>();

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "personnages")
    public Set<Apparaits> getApparaits() {
        return apparaits;
    }

    public void setApparaits(Set<Apparaits> apparaits) {
        this.apparaits = apparaits;
    }

    @Basic
    @Column(name = "nom")
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Basic
    @Column(name = "prenom")
    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    @Basic
    @Column(name = "profession")
    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    @Basic
    @Column(name = "sexe")
    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    @Basic
    @Column(name = "genre")
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Personnages)) return false;
        Personnages that = (Personnages) o;
        return nom.equals(that.nom) &&
                prenom.equals(that.prenom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, prenom);
    }

    @Override
    public String toString() {
        return "Personnages{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", profession='" + profession + '\'' +
                ", sexe='" + sexe + '\'' +
                ", genre='" + genre + '\'' +
                ", apparaits=" + apparaits +
                '}';
    }
}
