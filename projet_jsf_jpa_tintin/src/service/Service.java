package service;

import controleur.Client;
import modele.Albums;
import modele.Personnages;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import java.util.ArrayList;
import java.util.List;



public class Service {

    //Gestion d'un singleton

    private static Service service = new Service();

    private static EntityManagerFactory emf = JPAUtil.getemf();

    public static Service getSingleton(){
        return service;
    }

    public List<Albums> findAllAlbums(){
        List<Albums> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Albums.findAll", Albums.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }
    public List<Albums> findAlbumsByPers(Integer personnage){
        List<Albums> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Albums.findByPersonnage", Albums.class)
                .setParameter("personnage", personnage).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public List<Personnages> findPersonnagesOfAlbum(Albums albums){
        List<Personnages> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Personnages.findByAlbum", Personnages.class)
                .setParameter("album", albums).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }
    public Personnages findPersonnagesById(Integer id){
        Personnages liste = null;
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Personnages.findById", Personnages.class)
                .setParameter("id", id).getSingleResult();
        em.getTransaction().commit();
        em.close();
        return liste;
    }
    public List<Personnages> findAllPersonnage(){
        List<Personnages> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Personnages.findAll", Personnages.class)
                .getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;}


        public boolean autentification(Client client){
            return  (client.getNom().equals("scott") && client.getPassword().equals("tigger"));
        }

}
