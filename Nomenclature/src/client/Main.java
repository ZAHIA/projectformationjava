package client;

import static client.EnumProduit.*; /*doit fournir le chemin absolue our acceder a la classe->
                                    enumProduit afin de simplifier l'ecriture de produitBrut...*/

public class Main {
    public static void main(String[] args) {
        Usine usine = new UsineImpl();

        usine.creerProduit(produitBrut, "roue", 2.0);
        usine.creerProduit(produitBrut, "frein", 10.0);
        usine.creerProduit(produitBrut, "selle", 10.0);
        usine.creerProduit(produitSemiFini, "guidon");
        usine.creerProduit(produitSemiFini, "cadre");
        usine.creerProduit(produitFini, "velo");

        usine.ajoutComposant("frein", "guidon", 2,"lille");
        usine.ajoutComposant("selle", "cadre", 1,"lille");
        //usine.ajoutComposant("roue", "velo", 2, "lille");
        usine.ajoutComposant("guidon", "velo", 1,"lille");
        usine.ajoutComposant("cadre", "velo", 1,"lille");

        System.out.println(usine.getPrix("frein"));


        usine.afficherNomenclature("velo");
        usine.ruptureStock("frein");




    }
}
