package client;

public interface Usine {

    //regle metier

    void creerProduit(EnumProduit nature, String nomProduit);
    void creerProduit(EnumProduit nature, String nomProduit, double prix);

    void ajoutComposant(String nomProduitOrigine, String nomProduitDest, int quantite, String lieu);

    double getPrix( String nomProduit);

    void afficherNomenclature(String nomProduit);

    void ruptureStock( String nomProduit);


}
