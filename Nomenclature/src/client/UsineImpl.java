package client;

import modele.*;

import java.util.Map;
import java.util.TreeMap;

import static client.EnumProduit.produitBrut;

public class UsineImpl implements Usine {

    private Map<String, Produit> ensProduit = new TreeMap<>();

    @Override
    public void creerProduit(EnumProduit nature, String nomProduit) {
        if (nature==null || nomProduit==null) return;

        if (this.ensProduit.containsKey(nomProduit.toUpperCase())){
            System.out.println("Le produit existe déjà");
            return;
        }

        Produit nouv = null;

        switch (nature){
            case produitFini:
                nouv = new ProduitFini(nomProduit);
                break;
            case produitBrut:
                nouv = new ProduitBrut(nomProduit, 1.0);
                break;
            case produitSemiFini:
                nouv= new ProduitSemiFini(nomProduit);

                break;
            default:
                System.out.println("La nature de l'objet n'existe pas");
                return;
        }
        this.ensProduit.put(nomProduit.toUpperCase(), nouv);
    }

    @Override
    public void creerProduit(EnumProduit nature, String nomProduit, double prix) {
        if (nature != produitBrut) return;

        if (this.ensProduit.containsKey(nomProduit.toUpperCase())){
            System.out.println("Le produit existe déjà");
            return;
        }

        Produit nouv = null;

        nouv = new ProduitBrut(nomProduit, prix);

        this.ensProduit.put(nomProduit.toUpperCase(), nouv);
    }

    @Override
    public void ajoutComposant(String nomProduitOrigine, String nomProduitDest, int quantite, String lieu) {
        if (nomProduitOrigine == null) return;
        Produit produitOrigine = this.ensProduit.get(nomProduitOrigine.toUpperCase());

        if (nomProduitDest == null) return;
        Produit produitDest = this.ensProduit.get(nomProduitDest.toUpperCase());

        if (produitDest==null) return;

        try {
            produitDest.addComposant(produitOrigine,quantite,lieu);
        }catch (ExceptionMetier exceptionMetier){
            System.err.println(exceptionMetier.getMessage());
            return;
        }
    }

    @Override
    public double getPrix(String nomProduit) {
        if (nomProduit==null)return 0.0;
        Produit produit = this.ensProduit.get(nomProduit.toUpperCase());

        if(produit==null)return 0.0;
        return produit.getPrix();
    }

    @Override
    public void afficherNomenclature(String nomProduit) {
        if (nomProduit==null) return;

        if (this.ensProduit.containsKey(nomProduit.toUpperCase())){
            Produit pr = this.ensProduit.get(nomProduit.toUpperCase());
            pr.afficherNomenclature();
        }
    }

    @Override
    public void ruptureStock(String nomProduit) {
        if(nomProduit==null)return;

        if (this.ensProduit.containsKey(nomProduit.toUpperCase())){
            Produit pr = this.ensProduit.get(nomProduit.toUpperCase());
            pr.ruptureStock();
        }


    }
}
