package modele;


import java.util.*;

public abstract class Produit {
    //attributs d'instance
    private String nom;

    //association 1<-->* vers fabrication
    //Attention : Les objets Fabrication doivent être comparer (égalité)
    //Attention ils ne sont pas en static
    private  Set<Fabrication> composants = new HashSet<>();
    private  Set<Fabrication> composites = new HashSet<>();


    //getter setter
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = (nom==null ? "TEST" : nom.toLowerCase());
    }

    //equals
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Produit produit = (Produit) o;
        return nom.equals(produit.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom);
    }

    //constructeur
    public Produit(String nom) {
        this.setNom(nom);
    }
    public boolean estCompatible(Produit pr){
        return false;
    }

    //classe metier
    public void addComposant(Produit origine, int quantite, String lieu)
            throws ExceptionMetier
    {
        if (origine==null)
            throw new ExceptionMetier("Le composant est obligatoire");

        if(!this.estCompatible(origine)){
            throw new ExceptionMetier("Le composant n'est pas compatible");
        }

        Fabrication fab = new Fabrication(quantite, lieu, origine, this);
            origine.composites.add(fab);
            this.composants.add(fab);


    }

    //tostring


    @Override
    public String toString() {
        return this.getClass().getSimpleName()+
                "{" +
                "nom='" + nom + '\'' +
                '}';
    }

    //METIER

    public double getPrix(){
        double total = 0.0d;

        for (Fabrication f : this.composants){
            //System.out.println("composant " + f.quantite + " origine " + f.origine.getPrix());
            total = total + (f.quantite * f.origine.getPrix());
        }
        return total;
    }

    public void afficherNomenclature(){

            this.afficherNomenclature(0);


    }
   private void afficherNomenclature(int decalage){

       for (Fabrication f : this.composants){
        for (int i=0; i<decalage;i++)
            System.out.print("\t");
        System.out.println(f.origine.nom + " prix: " + f.origine.getPrix());
        f.origine.afficherNomenclature(decalage+1);
        }
    }

    public void ruptureStock(){
        this.ruptureStock(0);
    }

    private void ruptureStock(int decalage){
        String aux = "";
        for (int i=0; i<decalage;i++)
            aux = aux + "\t";
        for (Fabrication f : this.composites){
            System.out.println(aux + "Impossible de fabriquer : " + f.destination.nom);
            f.destination.ruptureStock(decalage+1);
        }
    }

    public static void main(String[] args)throws ExceptionMetier {
        Produit roue    = new ProduitBrut("roue", 2);
        Produit chassis = new ProduitSemiFini("chassis");
        Produit axe     = new ProduitBrut("axe", 1);
        Produit cadre   = new ProduitSemiFini("cadre");
        Produit velo    = new ProduitFini("velo");
        Produit frein   = new ProduitBrut("frein", 3);
        Produit selle   = new ProduitBrut("selle", 10);
        Produit guidon  = new ProduitSemiFini("guidon");

        chassis.addComposant(roue, 4, "lille");
        chassis.addComposant(axe, 2, "lille");

        cadre.addComposant(roue, 2, "ascq");
        cadre.addComposant(selle, 1, "ascq");
        guidon.addComposant(frein, 2, "ascq");
        velo.addComposant(guidon, 1, "ascq");
        velo.addComposant(cadre, 1,"ascq");

        System.out.println("prix: " + velo.getPrix());


        velo.afficherNomenclature();

        System.out.println("------------------------------------");
        frein.ruptureStock();

    }
}
