package modele;

public class ProduitFini extends Produit{
    //constructeur

    public ProduitFini(String nom) {
        super(nom);
    }

    @Override
    public boolean estCompatible(Produit pr) {
        if (pr==null) return false;

        return pr instanceof ProduitSemiFini;
    }
}
