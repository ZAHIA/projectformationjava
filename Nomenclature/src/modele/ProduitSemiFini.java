package modele;

public class ProduitSemiFini extends Produit {
    //constructeur

    public ProduitSemiFini(String nom) {
        super(nom);
    }

    @Override
    public boolean estCompatible(Produit pr) {
        if (pr==null) return false;

        return pr instanceof ProduitBrut;
    }

}

