<%@ page language="java" contentType="text/html" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
  <head>
    <title>Description d'un Produit</title>
  </head>
  <body bgcolor="white">

    <%-- Récuperer le produit du catalogue --%>
    <c:set var="produit" 
           value="${ens.catalogue[param.id]}" 
           scope="page" />

    <h1>${produit.nom}</h1>

    ${produit.descr} <br />
    ${produit.prix}

    <p />

    <c:url var="ajoutAuPanierURL" value="ajoutAuPanier.jsp" >
      <c:param name="id" value="${produit.id}" />
    </c:url>

    <a href="${ajoutAuPanierURL}">
      Ajouter ce produit au panier
    </a>

  </body>
</html>