package client;

import modele.Client;
import modele.Item;
import service.ServiceCart;
import service.ServiceItem;
import util.Achat;
import util.InfoCB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ManagedBean(name = "cartCtrl")
@SessionScoped
public class CartControleur implements Serializable {
    private String nom;
    private String prenom;
    private boolean authentifie;


    private Client client;
    private List<Achat> panier = new ArrayList<>();
    private InfoCB infoCB = null;

    private Map<Item, Integer> getContenuPanier(){
        Map<Item, Integer> panier = new HashMap<>();
        Item p1 = new Item();
        return panier;
    }

    public boolean isAuthentifie() {
        return authentifie;
    }

    public void setAuthentifie(boolean authentifie) {
        this.authentifie = authentifie;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public List<Achat> getPanier() {
        return panier;
    }

    public void setPanier(List<Achat> panier) {
        this.panier = panier;
    }

    public InfoCB getInfoCB() {
        return infoCB;
    }

    public void setInfoCB(InfoCB infoCB) {
        this.infoCB = infoCB;
    }

    public String doLogin(){
        this.client = ServiceCart.getSingleton().findOrCreate(this.nom, this.prenom);
        this.authentifie= true;
        return "showItems";
    }
    public String doHome(){
        this.client = null;
        this.authentifie= false;
        this.panier = null;
        return "showItems";
    }
    public List<Item> getEnsItems(){
        return ServiceItem.getSingleton().findAllItem();
    }

    private Item itemSelectionne;
    public String doSelectItem(Item item){
        this.itemSelectionne = item;
        this.panier.add(new Achat(item.getCodeBarre(),1));
        return "showPanier";
    }

    //------------------------------------------show panier -------------------------------------------
    public Map<Item, Integer> getValuePanier(){
        return ServiceCart.getSingleton().contenuDuPanierValue(panier);
    }

    Integer nouvQte;

    public void updateQuantite(ValueChangeEvent event){
        nouvQte = (Integer) event.getNewValue();
    }

}
