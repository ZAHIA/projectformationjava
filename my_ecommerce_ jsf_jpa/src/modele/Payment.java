package modele;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "payment")
public class Payment implements Serializable {
    private int id;
    private Double montant;
    private Date datePaiement;
    private String typePayment;
    private String creditCard;
    private Date creditCardExpiration;
    private Commande commandes;
    private Client client;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "montant")
    public Double getMontant() {
        return montant;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    @Basic
    @Column(name = "date_paiement")
    public Date getDatePaiement() {
        return datePaiement;
    }

    public void setDatePaiement(Date datePaiement) {
        this.datePaiement = datePaiement;
    }

    @Basic
    @Column(name = "type_payment")
    public String getTypePayment() {
        return typePayment;
    }

    public void setTypePayment(String typePayment) {
        this.typePayment = typePayment;
    }

    @Basic
    @Column(name = "credit_card")
    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    @Basic
    @Column(name = "credit_card_expiration")
    public Date getCreditCardExpiration() {
        return creditCardExpiration;
    }

    public void setCreditCardExpiration(Date creditCardExpiration) {
        this.creditCardExpiration = creditCardExpiration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Payment payment = (Payment) o;
        return id == payment.id &&
                Objects.equals(montant, payment.montant) &&
                Objects.equals(datePaiement, payment.datePaiement) &&
                Objects.equals(typePayment, payment.typePayment) &&
                Objects.equals(creditCard, payment.creditCard) &&
                Objects.equals(creditCardExpiration, payment.creditCardExpiration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, montant, datePaiement, typePayment, creditCard, creditCardExpiration);
    }

    @OneToOne(mappedBy = "payment")
    @Temporal(TemporalType.DATE)
    public Commande getCommandes() {
        return commandes;
    }

    public void setCommandes(Commande commandes) {
        this.commandes = commandes;
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Payment(Double montant, Date datePaiement,
                    String typePayment, String creditCard, Date creditCardExpiration){
        this.setMontant(montant);
        this.setDatePaiement(datePaiement);
        this.setTypePayment(typePayment);
        this.setCreditCard(creditCard);
        this.setCreditCardExpiration(creditCardExpiration);
    }

    public Payment() {
    }
}
