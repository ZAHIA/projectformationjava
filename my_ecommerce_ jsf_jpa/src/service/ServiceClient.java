package service;


import modele.Client;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class ServiceClient {
    //--------------------------------- G E S T I O N   D ' U N   S I N G L E T O N ------------------------------------
    private static ServiceClient serviceClient = new ServiceClient();

    private static EntityManagerFactory emf = JPAUtil.getemf();

    public static ServiceClient getSingleton(){
        return serviceClient;
    }

    //---------------------------------------------------- C R U D -----------------------------------------------------
    public List<Client> findAllClient(){
        List<Client> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        liste = em.createNamedQuery("Client.findAll", Client.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public Client findClientById(Integer id){
        Client client = new Client();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        client = em.find(Client.class, id);
        em.getTransaction().commit();
        em.close();
        return client;
    }


}
