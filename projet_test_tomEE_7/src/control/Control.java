package control;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class Control {
    private String nom = "michael";

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
