<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 25/02/2020
  Time: 13:16
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Choix d'un acteur</title>
</head>
<body>
    <form method="post" action="process?action=carriere">
        <fieldset>
            <legend>Choix d'un acteur</legend>
            <label>Acteur : </label>
            <select name="acteur">
                <c:forEach var="acteur" items="${ensActeurs}">
                    <option value="${acteur.nacteur}">
                        ${acteur.nom} ${acteur.prenom}
                    </option>
                </c:forEach>
            </select>
            <br>
            <input type="submit" value="Envoie">
        </fieldset>
    </form>
</body>
</html>
