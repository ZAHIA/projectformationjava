package control;

import dao.CinemaDAO;
import dto.ActeurDTO;
import dto.FilmDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/process")
public class CarriereServlet1 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    //navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //routage
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            //recupertaion de l'action
            String action = request.getParameter("action");
            System.out.println("test");
            //execution du traitement
            switch (action){
                case("init") :
                    doInit(request,response);
                    break;
                case ("carriere") :
                    doCarriere(request,response);
                    break;
                case ("infofilm") :
                    doInfoFilm(request,response);
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    protected void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        if (request.getSession().getAttribute("ensActeurs")==null){
            List<ActeurDTO> ensActeurs = CinemaDAO.getInstance().findAllActeurs();

            request.getSession().setAttribute("ensActeurs" , ensActeurs);
        }
        String url = "pages/afficheActeur.jsp";
        forward(url, request, response);
    }

    protected void doCarriere(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //recuperation du numero du film
        String choix = request.getParameter("acteur");
        int nacteur = Integer.parseInt(choix);
        //interogation du model afin de recuperer les film de cet acteur ainsi que l'acteur
        CinemaDAO facade = CinemaDAO.getInstance();
        List<FilmDTO> ensFilms = facade.findFilmsOfActeur(nacteur);
        ActeurDTO acteur = facade.findAllActeursById(nacteur);

        //sauvegarde de l'acteur et de la cariere dans le scope request
        request.setAttribute("acteur", acteur);
        request.setAttribute("carriere", ensFilms);

        //cinelatique
        String url = "pages/carriere.jsp";
        forward(url, request, response);
    }

    protected void doInfoFilm(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //recuperation du numero de film
        String choix = request.getParameter("nfilm");
        int nfilm = Integer.parseInt(choix);

        //Interrogation du model pour recuperer le DTO film
        CinemaDAO facade = CinemaDAO.getInstance();
        FilmDTO film = facade.findFilmById(nfilm);

        // sauvegarde du film
        request.setAttribute("film", film);

        //cinematique
        String url = "pages/afficheInfoFilm.jsp";
        forward(url, request, response);
    }
}
