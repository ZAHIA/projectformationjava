package metier;

public class Login {

    private String nom = "";
    private String password = "";

    //GETTER SETTER
    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    //metier
    public String getMsgErreur(){
        return (this.isConnu() ? "" : "Inconnu");
    }

    public boolean isConnu(){
        return (nom.equals("michael") && password.equals("tiger"));
    }


    //Constructeur


    public Login() {
        super();
    }
}
