package modele;

import javax.persistence.*;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "commande")
public class Commande {
    private int id;
    private Date datecde;
    private Client client;
    private Payment payment;
    private List<DetailCde> detailCdes = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "datecde")
    @Temporal(TemporalType.DATE)
    public Date getDatecde() {
        return datecde;
    }
    public void setDatecde(Date datecde) {
        this.datecde = datecde;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Commande commande = (Commande) o;
        return id == commande.id &&
                Objects.equals(datecde, commande.datecde);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, datecde);
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "id")
    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    @OneToOne
    @JoinColumn(name = "payment_id", referencedColumnName = "id")
    public Payment getPayment() {
        return payment;
    }
    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    @OneToMany(mappedBy = "commande")
    public List<DetailCde> getDetailCdes() {
        return detailCdes;
    }
    public void setDetailCdes(List<DetailCde> detailCdes) {
        this.detailCdes = detailCdes;
    }

    public Commande(Date datecde) {
        this.setDatecde(datecde);
    }

    public Commande() {
    }
}
