package modele;

import javax.persistence.*;
import java.util.Objects;
@Entity
@Table(name = "detail_cde")
public class DetailCde {
    private int id;
    private int qte;
    private Commande commande;
    private Item item;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "qte")
    public int getQte() {
        return qte;
    }
    public void setQte(int qte) {
        this.qte = qte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetailCde detailCde = (DetailCde) o;
        return id == detailCde.id &&
                qte == detailCde.qte;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, qte);
    }

    @ManyToOne
    @JoinColumn(name = "cde_id", referencedColumnName = "id")
    public Commande getCommande() {
        return commande;
    }
    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @ManyToOne
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    public Item getItem() {
        return item;
    }
    public void setItem(Item item) {
        this.item = item;
    }
}
