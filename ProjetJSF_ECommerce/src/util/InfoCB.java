package util;

import java.io.Serializable;
import java.util.Date;

public class InfoCB implements Serializable {
    private String numeroCarte;
    private Date dateExpirationCarte;

    public String getNumeroCarte() {
        return numeroCarte;
    }
    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }
    public Date getDateExpirationCarte() {
        return dateExpirationCarte;
    }
    public void setDateExpirationCarte(Date dateExpirationCarte) {
        this.dateExpirationCarte = dateExpirationCarte;
    }

    public InfoCB(String numeroCarte, Date dateExpirationCarte) {
        this.setNumeroCarte(numeroCarte);
        this.setDateExpirationCarte(dateExpirationCarte);
    }
}
