package service;


import modele.*;
import util.Achat;
import util.CardExpiredException;
import util.InfoCB;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.*;

public class ServiceCart {
    private static ServiceCart service = new ServiceCart();

    public static ServiceCart getSingleton() {
        return service;
    }

    private EntityManagerFactory emf = JPAUtil.getEmf();


    // ----------------- Creation ou localisation du client
    public Client findOrCreate(String nom, String prenom) {
        Client client;
        EntityManager em = emf.createEntityManager();
        try {
            client = em.createNamedQuery("Client.findByNomAndPrenom", Client.class)
                    .setParameter("nom", nom).setParameter("prenom", prenom).getSingleResult();
        } catch (Exception e) {
            client = new Client(nom, prenom);
            em.getTransaction().begin();
            em.persist(client);
            em.getTransaction().commit();
        }
        em.close();
        return client;
    }

    public List<Client> findAllClient() {
        EntityManager em = emf.createEntityManager();
        return em.createNamedQuery("Client.findAll", Client.class).getResultList();
    }

    public List<Item> contenuDuPanier(List<Achat> panier) {
        return null;
    }

    public Map<Item, Integer> contenuDuPanierValue(List<Achat> panier) {
        EntityManager em = emf.createEntityManager();
        Map<Item, Integer> liste = new HashMap<>();
        for (Achat a : panier) {
            Item item = em.find(Item.class, a.getCode());
            liste.put(item, a.getQuantite());
        }
        em.close();
        return liste;
    }

    public Double valeurDuPanier(List<Achat> panier) {
        double total = 0.0d;
        EntityManager em = emf.createEntityManager();
        for (Achat a : panier) {
            Item item = em.find(Item.class, a.getCode());
            total = total + a.getQuantite() * item.getPrix();
        }
        em.close();
        return total;
    }

    public void validerPanier(Client client, List<Achat> panier, InfoCB infoCB) throws CardExpiredException {
        if (infoCB.getDateExpirationCarte().before(new Date()))
            throw new CardExpiredException("date de validité dépassée");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        client = em.merge(client);
        Date dateDuJour = new Date();

        Commande commande = new Commande(dateDuJour);
        em.persist(commande);
        commande.setClient(client);
        client.getCommandes().add(commande);

        double total = 0.0d;
        for (Achat a : panier) {
            Item item = em.find(Item.class, a.getCode());
            total = total + a.getQuantite() * item.getPrix();
        }

        Payment payment = new Payment(total, dateDuJour, "CB", infoCB.getNumeroCarte(), infoCB.getDateExpirationCarte());
        payment.setClient(client);
        payment.setCommande(commande);

        em.persist(payment);

        client.getPayments().add(payment);
        commande.setPayment(payment);

        for (Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            DetailCde dc = new DetailCde();
            dc.setItem(item);
            dc.setCommande(commande);
            dc.setQte(achat.getQuantite());
            em.persist(dc);

            commande.getDetailCdes().add(dc);
        }

        em.getTransaction().commit();
        em.close();
    }
}
