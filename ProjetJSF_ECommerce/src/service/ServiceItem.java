package service;


import modele.Client;
import modele.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.List;

public class ServiceItem {
    private static ServiceItem service = new ServiceItem();
    public static ServiceItem getSingleton() {
        return service;
    }
    private EntityManagerFactory emf = JPAUtil.getEmf();

    public List<Client> findAllClient() {
        List<Client> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Client.findAll", Client.class).getResultList();
        em.getTransaction().commit();
        return liste;
    }

    public Client findClientById(int id) {
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        Client c = em.find(Client.class, id);
        em.getTransaction().commit();
        return c;
    }

    public List<Item> findAllItem() {
        List<Item> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();

        em.getTransaction().begin();
        liste = em.createNamedQuery("Item.findAll", Item.class).getResultList();
        em.getTransaction().commit();
        return liste;
    }
}
