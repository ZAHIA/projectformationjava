package controller;

import modele.Client;
import modele.Item;
import service.ServiceCart;
import service.ServiceItem;
import util.Achat;
import util.InfoCB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import java.io.Serializable;
import java.util.*;

@ManagedBean(name= "cartCtrl")
@SessionScoped
public class CartController implements Serializable {
    private Client client;
    private List<Achat> panier;
    private InfoCB infoCB;
    private Boolean authentifie;

//    public Map<Item, Integer> getContenuPanier(){
//        Map<Item, Integer> panier = new HashMap<>();
//        Item p1 = new Item();
//        return panier;
//    }

    public Client getClient() {
        return client;
    }
    public void setClient(Client client) {
        this.client = client;
    }
    public List<Achat> getPanier() {
        return panier;
    }
    public void setPanier(List<Achat> panier) {
        this.panier = panier;
    }
    public InfoCB getInfoCB() {
        return infoCB;
    }
    public void setInfoCB(InfoCB infoCB) {
        this.infoCB = infoCB;
    }
    public Boolean getAuthentifie() {
        return authentifie;
    }
    public void setAuthentifie(Boolean authentifie) {
        this.authentifie = authentifie;
    }


    //Login
    private String nom;
    private String prenom;


    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String doLogin(){
        this.client = ServiceCart.getSingleton().findOrCreate(this.nom,this.prenom);
        this.authentifie = true;
        this.panier = new ArrayList<>();

        return "showItems";
    }

    public String doHome(){
        this.client = null;
        this.authentifie = false;

        return "index";
    }

    //---------------------- ShowItems
    public List<Item> getEnsItems(){
        return ServiceItem.getSingleton().findAllItem();
    }

    private Item itemSelectionne;

    public String doSelectItem(Item item){
        this.itemSelectionne = item;
        this.panier.add(new Achat(item.getId(),item.getTitre(), item.getCodeBarre(), item.getPrix(), 1));
        System.out.println(item.getId()+"titre"+item.getTitre()+" code barre "+item.getCodeBarre());
        return "showPanier";
    }

    //--------------------- ShowPanier
    private Integer nouvQte;
    private Achat achatSelectionne;



    public Double getTotalPanier(){
        return ServiceCart.getSingleton().valeurDuPanier(panier);
    }

    public void updateQte(ValueChangeEvent event){
        nouvQte = (Integer) event.getNewValue();
    }

    public String doModifierAchat(Achat achat){
        this.achatSelectionne = achat;
        achatSelectionne.setQuantite(nouvQte);
        this.panier.remove(achat);
        this.panier.add(achatSelectionne);
        return "showPanier";
    }
    public String doSupprimer(Achat achat){
        this.achatSelectionne = achat;
        this.panier.remove(achat);
        if (this.panier.isEmpty()) return "showItems";
        return "showPanier";
    }

}
