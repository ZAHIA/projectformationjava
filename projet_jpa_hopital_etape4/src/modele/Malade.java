package modele;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value="Mal")
public class Malade extends Personne implements Serializable {
    private static final long serialVersionUID = 1L;


    @Embedded
    private Adresse adresse = new Adresse(10,"rue des Tulipes", "Lille");

    protected Malade() {

    }
    public Malade(String nom, String prenom, Adresse adresse) {
        super(nom, prenom);
        this.adresse = adresse;
    }
    public Malade(String nom, String prenom) {
        super(nom, prenom);
    }

    @Override
    public String toString() {
        return super.toString() + "\n" +
                "adresse=" + adresse +
                '}';
    }
}
