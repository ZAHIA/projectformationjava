package service;

import modele.*;
import util.Achat;
import util.CardExpiredException;
import util.InfoCB;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.Serializable;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.*;

public class ServiceCart implements Serializable {
    //--------------------------------- G E S T I O N   D ' U N   S I N G L E T O N ------------------------------------
    private static ServiceCart serviceCart = new ServiceCart();

    private static EntityManagerFactory emf = JPAUtil.getemf();

    public static ServiceCart getSingleton(){
        return serviceCart;
    }

    //---------------------------------------------------- C R U D -----------------------------------------------------
    public Client findOrCreate(String nom, String prenom){
        Client client = null;
        EntityManager em = emf.createEntityManager();
        try {
            client = em.createNamedQuery("Client.findClientByNomAndPrenom", Client.class)
                    .setParameter("nom",nom)
                    .setParameter("prenom", prenom)
                    .getSingleResult();

        }catch (Exception ex){
            client = new Client(nom, prenom);
            em.getTransaction().begin();
            em.persist(client);
            em.getTransaction().commit();
        }
        em.close();
        return client;
    }

    public List<Client> findAllClient(){
        List<Client> liste = new ArrayList<>();
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        liste = em.createNamedQuery("Client.findAll", Client.class).getResultList();
        em.getTransaction().commit();
        em.close();
        return liste;
    }

    public Map<Item, Integer> contenuDuPanierValue(List<Achat> panier){
        Map<Item, Integer> contenu = new HashMap<>();
        EntityManager em = emf.createEntityManager();
        for (Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            contenu.put(item, achat.getQuantite());
        }
        em.close();
        return contenu;
    }

    public Double valeurDuPanier(List<Achat> panier){
        Double total = 0.0d;
        EntityManager em = emf.createEntityManager();
        for (Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            total = total + achat.getQuantite()* item.getPrix();
        }
        em.close();
        return total;
    }

    public void validerPanier(Client client, List<Achat> panier, InfoCB infoCB) throws Exception{
        if(infoCB.getDateExpiration().before(new java.util.Date()))
            throw new CardExpiredException("Date de validitée dépassée");

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        client=em.merge(client);
        Date dateDuJour = new Date();
        Commande commande = new Commande(dateDuJour);
        em.persist(commande);
        commande.setClient(client);
        client.getCommandes().add(commande);

        Double total = 0.0d;
        for (Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            total = total + achat.getQuantite()* item.getPrix();
        }

        Payment payment = new Payment(total, dateDuJour, "CB", infoCB.getNumeroCarte(), infoCB.getDateExpiration());
        payment.setClient(client);
        payment.setCommandes(commande);
        em.persist(payment);

        client.getPayments().add(payment);
        commande.setPayment(payment);

        for (Achat achat : panier){
            Item item = em.find(Item.class, achat.getCode());
            DetailCde detailCde = new DetailCde();
            detailCde.setItem(item);
            detailCde.setCommande(commande);
            detailCde.setQte(achat.getQuantite());

            em.persist(detailCde);

            commande.getDetailCdes().add(detailCde);
        }
        em.getTransaction().commit();
        em.close();
    }

}
