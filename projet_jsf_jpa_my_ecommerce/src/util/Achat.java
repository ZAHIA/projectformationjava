package util;

import java.io.Serializable;

public class Achat implements Serializable {
    private int code;
    private int quantite;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    public Achat(int code, int quantite) {
        this.setCode(code);
        this.setQuantite(quantite);
    }

    public Achat(){
    }
}
