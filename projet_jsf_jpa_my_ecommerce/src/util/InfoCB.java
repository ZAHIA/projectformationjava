package util;

import java.io.Serializable;
import java.util.Date;

public class InfoCB implements Serializable {
    private String numeroCarte;
    private Date dateExpiration;

    public String getNumeroCarte() {
        return numeroCarte;
    }

    public void setNumeroCarte(String numeroCarte) {
        this.numeroCarte = numeroCarte;
    }

    public Date getDateExpiration() {
        return dateExpiration;
    }

    public void setDateExpiration(Date dateExpiration) {
        this.dateExpiration = dateExpiration;
    }

    public InfoCB(String numeroCarte, Date dateExpiration) {
        this.numeroCarte = numeroCarte;
        this.dateExpiration = dateExpiration;
    }

    public InfoCB(){
    }
}
