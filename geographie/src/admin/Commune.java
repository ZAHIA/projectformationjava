package admin;

public class Commune extends Zone{
    //attributs d'instance
    public String maire;
    public int population;

    //getter setter


    public String getMaire() {
        return maire;
    }

    public void setMaire(String maire) {
        this.maire = (maire==null ? "MAIRE" : maire.toUpperCase());
    }

    @Override
    public int getPopulation() { //permet de terminer la recurcivite
        return population;
    }

    public void setPopulation(int population) {
        this.population = (population <= 0 ? 1 : population);
    }

    //constructeur


    public Commune(String nom, String maire, int population) {
        super(nom);
        this.setMaire(maire);
        this.setPopulation(population);
    }

    @Override
    public String toString() {
        return super.toString()+
                "\n{" +
                "president ='" + maire + '\'' +
                "population = " + population +
                '}';
    }

}
