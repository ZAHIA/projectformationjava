package modele;

import java.util.*;

public class Album implements Comparable<Album>{
    //--------------------- attribut d'instance -------------------------
    public String titre;
    public Integer annee;
    public Album suite;

    public List<Personnage> perso = new LinkedList<>();


    //----------------------- Setter ------------------------------------
    public void setTitre(String titre) {
        this.titre = (titre==null?"NONE":titre.toUpperCase());
    }

    public void setAnnee(Integer annee) {
        this.annee = (annee==null? 1950:annee);
    }

    public void setSuite(Album suite) {
        this.suite = suite;
    }

    //-------------------------- Getter ---------------------------------
    public String getTitre() {
        return titre;

    }

    public Integer getAnnee() {
        return annee;
    }

    public Album getSuite() {
        return suite;
    }

    //-------------------------- Constructeur ---------------------------
    public Album(String titre, Integer annee, Album suite) {
        this.setTitre(titre);
        this.setAnnee(annee);
        this.setSuite(suite);
    }

    //---------------------------- to string -----------------------------
    @Override
    public String toString() {
        return "Album " +
                "titre : '" + titre + '\'' +
                ", annee = " + annee +
                (suite==null ? "" : " suite = " + suite.titre) +
                "\n";
    }

    //------------------------------ equals ------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Album)) return false;
        Album album = (Album) o;
        return titre.equals(album.titre) &&
                annee.equals(album.annee) &&
                suite.equals(album.suite);
    }

    @Override
    public int hashCode() {
        return Objects.hash(titre, annee, suite);
    }

    //----------------------------- Comparable ---------------------------
    @Override
    public int compareTo(Album autre) {
        return titre.compareTo(autre.titre);
    }

    public void addPerso(Personnage personnage) {
        perso.add(personnage);
    }
}
