package modele;

import javafx.scene.control.skin.CellSkinBase;

import java.util.*;

import static modele.EnumNature.*;
import static modele.EnumSexe.*;

public class TintinMania {
    public static Set<Album> mesTintins = new TreeSet<>();
    public static Set<Personnage> mesPersonnages = new TreeSet<>();


    static Set<Album> listeAlbumsDunPersonnage(String nom, String prenom){
        //List<Album> liste1 = new LinkedList<>();
        if(nom==null || prenom==null)
            System.out.println("Le nom et le prenom e peuvent pas etre null");


        for (Personnage p:mesPersonnages) {
            if(nom.toUpperCase().equals((p.getNom())) && prenom.toUpperCase().equals(p.getPrenom())) {
                return p.getApparait();
            }
        }
        return null;
        }


    static List<Album> listeAlbumParTitre(){
        List<Album> liste1 = new LinkedList<>(mesTintins);
        Collections.sort(liste1);

        return liste1;
    }

    static List<Album> listeAlbumParAnnee(){
        class AlbumComparator implements Comparator<Album> {
            @Override
            public int compare(Album a1, Album a2) {
                Integer annee = a1.getAnnee().compareTo(a2.getAnnee());

                if(annee != 0 )
                    return annee;
                return a1.getTitre().compareTo(a2.getTitre());
            }
        }
        List<Album> liste1 = new LinkedList<>(mesTintins);
        Collections.sort(liste1,new AlbumComparator());

        return liste1;
    }

    static List<Album> listeAlbumParAnneeBis(){
        class AlbumComparatorBis implements Comparator<Album> {
            @Override
            public int compare(Album a1, Album a2) {
                Integer annee = a1.getAnnee().compareTo(a2.getAnnee());

                if(annee != 0 )
                    return -annee;
                return -a1.getTitre().compareTo(a2.getTitre());
            }
        }
        List<Album> liste1 = new LinkedList<>(mesTintins);
        Collections.sort(liste1,new AlbumComparatorBis());

        return liste1;
    }
    static List<Personnage> listePersonnageParSexe(){
        class AlbumComparatorSexe implements Comparator<Personnage> {
            @Override
            public int compare(Personnage a1, Personnage a2) {
                Integer sexe = a1.getSexe().compareTo(a2.getSexe());
                Integer nome = a1.getNom().compareTo(a2.getNom());
                if(sexe != 0 )
                    return sexe;
                else if (nome != 0)
                    return a1.getNom().compareTo(a2.getNom());
                return a1.getPrenom().compareTo(a2.getPrenom());
            }
        }
        List<Personnage> liste3 = new LinkedList<>(mesPersonnages);
        Collections.sort(liste3,new AlbumComparatorSexe());

        return liste3;
    }

    static List<Personnage> listePersonnageParApparition(){
        class AlbumComparatorPersApp implements Comparator<Personnage> {
            @Override
            public int compare(Personnage a1, Personnage a2) {
                Integer iA1 = a1.apparait.size();
                Integer iA2 = a2.apparait.size();
                Integer app = iA1.compareTo(iA2);
                Integer nome = a1.getNom().compareTo(a2.getNom());

                if(app != 0 )
                    return -app;
                else if (nome != 0)
                    return a1.getNom().compareTo(a2.getNom());
                return a1.getPrenom().compareTo(a2.getPrenom());
            }
        }
        List<Personnage> liste1 = new LinkedList<>(mesPersonnages);
        Collections.sort(liste1,new AlbumComparatorPersApp());

        return liste1;
    }

    //permet de creer et affecter les personnages aux albums
    private  void chargerAlbumsETPersonnages(){
        Album a1 = new Album("Tintin au pays des Soviets", 1930,null);
        Album a2 = new Album("Tintin au Congo", 1931,null);
        Album a3 = new Album("Tintin en Amérique", 1932,null);
        Album a4 = new Album("Les Cigares du pharaon", 1934,null);
        Album a5 = new Album("Le Lotus bleu", 1936,a4);
        Album a6 = new Album("L'Oreille cassee", 1937,null);
        Album a7 = new Album("L'Ile noire", 1938,null);
        Album a8 = new Album("Le Sceptre d'Ottokar", 1939,null);
        Album a9 = new Album("Le Crabe aux pinces d'or", 1941,null);
        Album a10= new Album("L'Etoile mysterieuse", 1942,null);
        Album a11= new Album("Le Secret de la Licorne", 1943,null);
        Album a12= new Album("Le Trésor de Rackham le Rouge", 1944,a11);
        Album a13= new Album("Les 7 boules de cristal", 1948,null);
        Album a14= new Album("Le Temple du soleil", 1950,a13);

        Personnage p1 = new Personnage("Tintin","","Reporter",HOMME, GENTIL);
        Personnage p2 = new Personnage("Milou","",null,HOMME,GENTIL);
        Personnage p3 = new Personnage("Haddock","Archibald","Capitaine",HOMME,GENTIL);
        Personnage p4 = new Personnage("Tournesol","Tryphon","Professeur",HOMME,GENTIL);
        Personnage p5 = new Personnage("Dupond","","Detective",HOMME,GENTIL);
        Personnage p6 = new Personnage("Dupont","","Detective",HOMME,GENTIL);
        Personnage p7 = new Personnage("CASTAFIORE","Bianca","Cantatrice",FEMME,GENTIL);

        p1.participe(a1);        p1.participe(a2);        p1.participe(a3);        p1.participe(a4);
        p1.participe(a5);        p1.participe(a6);        p1.participe(a7);        p1.participe(a8);
        p1.participe(a9);        p1.participe(a10);        p1.participe(a11);        p1.participe(a12);
        p1.participe(a13);        p1.participe(a14);        p2.participe(a1);        p2.participe(a2);
        p2.participe(a3);        p2.participe(a4);        p2.participe(a5);        p2.participe(a6);
        p2.participe(a7);        p2.participe(a8);        p2.participe(a9);        p2.participe(a10);
        p2.participe(a11);        p2.participe(a12);        p2.participe(a13);        p2.participe(a14);
        p3.participe(a9);        p3.participe(a10);        p3.participe(a11);        p3.participe(a12);
        p3.participe(a13);        p3.participe(a14);        p4.participe(a12);        p4.participe(a13);
        p4.participe(a14);        p5.participe(a4);        p5.participe(a5);        p5.participe(a7);
        p5.participe(a8);        p5.participe(a9);        p5.participe(a11);        p5.participe(a12);
        p5.participe(a13);        p5.participe(a14);        p6.participe(a4);        p6.participe(a5);
        p6.participe(a7);        p6.participe(a8);        p6.participe(a9);        p6.participe(a11);
        p6.participe(a12);        p6.participe(a13);        p6.participe(a14);        p7.participe(a8);
        mesTintins.add(a1);        mesTintins.add(a2);        mesTintins.add(a3);        mesTintins.add(a4);
        mesTintins.add(a5);        mesTintins.add(a6);        mesTintins.add(a7);        mesTintins.add(a8);
        mesTintins.add(a9);        mesTintins.add(a10);        mesTintins.add(a11);        mesTintins.add(a12);
        mesTintins.add(a13);        mesTintins.add(a14);
        mesPersonnages.add(p1);        mesPersonnages.add(p2);        mesPersonnages.add(p3);        mesPersonnages.add(p4);
        mesPersonnages.add(p5);        mesPersonnages.add(p6);        mesPersonnages.add(p7);
    }

    //--------------------- tostring ---------------------------------
    @Override
    public String toString() {
        return "TintinMania\n" +
                "mesTintins \n" + mesTintins +
                "\n mesPersonnages=" + mesPersonnages +
                "";
    }

    //---------------------- equals --------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TintinMania)) return false;
        TintinMania that = (TintinMania) o;
        return mesTintins.equals(that.mesTintins) &&
                mesPersonnages.equals(that.mesPersonnages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mesTintins, mesPersonnages);
    }

    public static void main(String[] args) {
        TintinMania t1 = new TintinMania();
        t1.chargerAlbumsETPersonnages();
        System.out.println("---------------L I S T E  P A R  T I T R E -----------------------");
        System.out.println(t1.listeAlbumParTitre());
        System.out.println("--------------- A L B U M  T I N T I N -----------------------");
        System.out.println(t1.listeAlbumsDunPersonnage("Tintin", ""));
        System.out.println("--------------- A L B U M  T O U R N E S O L -----------------------");
        System.out.println(t1.listeAlbumsDunPersonnage("TOURNESOL", "tryphon"));
        System.out.println("---------------L I S T E  P A R  A N N E E -----------------------");
        System.out.println(t1.listeAlbumParAnnee());
        System.out.println("---------------L I S T E  P A R  A N N E E -----------------------");
        System.out.println(t1.listeAlbumParAnneeBis());
        System.out.println("---------------L I S T E  P A R  S E X E -----------------------");
        System.out.println(t1.listePersonnageParSexe());
        System.out.println("---------------L I S T E  P A R  A P P A R I T I O N -----------------------");
        System.out.println(t1.listePersonnageParApparition());

    }


}