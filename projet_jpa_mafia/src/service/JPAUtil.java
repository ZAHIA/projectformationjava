package service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtil {
    //Gestion d'un singleton

    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("mafiaPU");


    public static EntityManagerFactory getemf(){
        return emf;
    }
}
