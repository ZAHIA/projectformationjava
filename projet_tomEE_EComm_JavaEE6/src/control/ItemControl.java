package control;

import modele.Item;
import service.ItemSS;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import java.util.*;

@ManagedBean(name = "itemCtrl")
public class ItemControl {

    @EJB
    private ItemSS facadeItem;

    private Item newItem = new Item();

    public Item getNewItem() {
        return newItem;
    }

    public void setNewItem(Item newItem) {
        this.newItem = newItem;
    }

    public List<Item> getEnsItems(){
        return facadeItem.findAllItems();
    }

    public String doAddItem(){
        facadeItem.create(newItem);
        return "showCatalogue";
    }

    public String doRemoveItem(Item item){
        facadeItem.delete(item);
        return "showCatalogue";
    }
}
