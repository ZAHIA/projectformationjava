package modele;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;

@Entity
@NamedQueries({
        @NamedQuery(name = "Item.findAll",
        query="SELECT item FROM Item item ORDER BY item.categorie, item.titre"
        ),
        @NamedQuery(name = "Item.findAllOfCategorie",
                query="SELECT item FROM Item item WHERE item.categorie=:categorie ORDER BY item.titre"
        ),
        @NamedQuery(name = "Item.findAllCategorie",
                query="SELECT DISTINCT item.categorie FROM Item item WHERE item.categorie=:categorie ORDER BY item.categorie"
        ),
        @NamedQuery( name = "Item.findById",
                query="SELECT item FROM Item item WHERE item.id=:id"
        )
})
@Table(name = "item")
public class Item {
    private int id;
    private String categorie;
    private String titre;
    private Double prix;
    private int codeBarre;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "categorie")
    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "prix")
    public Double getPrix() {
        return prix;
    }

    public void setPrix(Double prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "code_barre")
    public int getCodeBarre() {
        return codeBarre;
    }

    public void setCodeBarre(int codeBarre) {
        this.codeBarre = codeBarre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Item)) return false;
        Item item = (Item) o;
        return id == item.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Item{" +
                "id=" + id +
                ", categorie='" + categorie + '\'' +
                ", titre='" + titre + '\'' +
                ", prix=" + prix +
                ", codeBarre=" + codeBarre +
                '}';
    }
}
