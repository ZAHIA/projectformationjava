package control;

import javax.inject.Named;

@Named
public class control {
    private String nom = "michael";

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
