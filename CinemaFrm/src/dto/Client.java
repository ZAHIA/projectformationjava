package dto;


import java.io.Serializable;

public class Client implements Serializable {
    private int codeClient;
    private String nom;
    private String prenom;
    private String adresse;
    private long anciennete;

    // -------------------------------- G E T T E R   S E T T E R -------------------------------------
    public int getCodeClient() {
        return codeClient;
    }

    public void setCodeClient(int codeClient) {
        this.codeClient = codeClient;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public long getAnciennete() {
        return anciennete;
    }

    public void setAnciennete(long anciennete) {
        this.anciennete = anciennete;
    }

    //-------------------------------------- C O N S T R U C T E U R --------------------------------

    public Client(int codeClient, String nom, String prenom, String adresse, long anciennete) {
        super();
        this.codeClient = codeClient;
        this.nom = nom;
        this.prenom = prenom;
        this.adresse = adresse;
        this.anciennete = anciennete;
    }


    //to string
    @Override
    public String toString() {
        return "Client{" +
                "codeClient=" + codeClient +
                ", nom='" + nom + '\'' +
                ", prenom='" + prenom + '\'' +
                ", adresse='" + adresse + '\'' +
                ", anciennete=" + anciennete +
                '}';
    }
}
