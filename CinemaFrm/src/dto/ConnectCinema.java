package dto;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectCinema {
    //url de connection
    private final static String URL = "jdbc:mysql://localhost:3306/cinema";
    //utilisatuer de connection
    private final static String USER = "michael";
    //mot de passe de connection
    private final static String PW = "mdppopmichael";

    //SINGLETON
    private static Connection INSTANCE;

    //constructeur privé

    private ConnectCinema() {
        try{
            INSTANCE = DriverManager.getConnection(URL,USER,PW);
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    public static Connection getInstance(){
        if (INSTANCE == null){
            new ConnectCinema();
        }
        return INSTANCE;
    }

    public static void main(String[] args) {
        ConnectCinema.getInstance();
    }
}
