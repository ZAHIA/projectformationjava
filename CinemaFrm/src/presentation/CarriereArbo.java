package presentation;

import dto.FilmDTO;
import dto.GenreDTO;
import metier.DAOMetier;
import metier.IDAOMetier;

import javax.swing.*;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.io.FileInputStream;

import static presentation.CarriereActeur.sombre;

public class CarriereArbo {
    private JPanel rootPanel;
    private JTree treeGenre;
    private JTextArea textAreaInfo;

    //variable d'instance liaison avec la partie metier
    private IDAOMetier cinema;

    private TreeModel creationModele(){
        DefaultMutableTreeNode racine = new DefaultMutableTreeNode("genre");

        try {
            for (GenreDTO g : cinema.ensGenres()){
                DefaultMutableTreeNode noeud = new DefaultMutableTreeNode(g);
                racine.add(noeud);
                for (FilmDTO film : cinema.ensFilmsDuGenre(g.getNgenre())){
                    noeud.add(new DefaultMutableTreeNode(film));
                }
            }
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
        return new DefaultTreeModel(racine);
    }

    private void init(){
        try {
            cinema = DAOMetier.getInstance();

        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }

    public CarriereArbo(){
        init();

        //color

        treeGenre.setBackground(sombre);
        textAreaInfo.setBackground(sombre);
        textAreaInfo.setForeground(Color.white);

        treeGenre.setModel(creationModele());

        treeGenre.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        treeGenre.addTreeExpansionListener(new TreeExpansionListener() {
            @Override
            public void treeExpanded(TreeExpansionEvent treeExpansionEvent) {
                try{ DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) treeExpansionEvent.getPath().getLastPathComponent();

                    Object obj = noeud.getUserObject();
                    if(obj instanceof GenreDTO) {

                        GenreDTO genre = (GenreDTO) noeud.getUserObject();

                        textAreaInfo.setText("Nombre de film du genre " + genre + " : " + cinema.nbreFilmDuGenre(genre.getNgenre()));
                    }

                }catch (Exception e){
                    System.err.println(e.getMessage());
                }
            }

            @Override
            public void treeCollapsed(TreeExpansionEvent treeExpansionEvent) {

            }
        });
        treeGenre.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent treeSelectionEvent) {
                if (treeGenre.getSelectionPath() != null && treeGenre.getSelectionPath().getPathCount()==3){
                    DefaultMutableTreeNode noeud = (DefaultMutableTreeNode) treeGenre.getLastSelectedPathComponent();

                    if (noeud.isLeaf()){
                        try{
                            FilmDTO film = (FilmDTO) noeud.getUserObject();
                            textAreaInfo.setText("Informatiopn sur le film : " + film.getTitre() + "\n\n"
                                                + "Réalisateur : " + film.getNomRealisateur() + "\n"
                                                + "Acrteur principal : " + film.getNomActeur());
                        }catch (Exception ex){
                            System.err.println(ex.getMessage());
                        }
                    }
                }
            }
        });
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("CarriereArbo");
        frame.setContentPane(new CarriereArbo().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
