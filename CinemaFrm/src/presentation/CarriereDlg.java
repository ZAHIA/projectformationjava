package presentation;

import metier.ScrollingResultSetTableModel;

import javax.swing.*;
import java.awt.event.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CarriereDlg extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JPanel carrierePanel;
    private JTable tableCarriere;

    private PreparedStatement stmt;
    private ResultSet rs;

    private ScrollingResultSetTableModel model;

    //requete SQL : carriere d'un acteur
    private static String QUERY =   " select TITRE, NATURE, nom as Pays, REALISATEUR, SORTIE, ENTREES "
                                +   " FROM FILM "
                                +   " JOIN GENRE USING (ngenre) "
                                +   " JOIN PAYS USING (npays) "
                                +   " WHERE NACTEURPRINCIPAL = ? "
                                +   " ORDER BY TITRE ";

    private void init(){
        try{
            stmt = dto.ConnectCinema.getInstance().prepareStatement(QUERY,
            ResultSet.TYPE_SCROLL_INSENSITIVE,
            ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
    }

    public void show(int nacteur) {
        try {
            if (rs != null){
                rs.close();
        }
            stmt.setInt(1, nacteur);
            rs=stmt.executeQuery();

            model = new ScrollingResultSetTableModel(rs);
            this.tableCarriere.setModel(model);

            this.setVisible(true);
    }catch (SQLException e){
            System.err.println("Erreur" + e);
        }
    }

    public CarriereDlg() {
        init();
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });



        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() {
        // add your code here
        this.setVisible(false);
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        CarriereDlg dialog = new CarriereDlg();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

}
