package presentation;

import dto.FilmDTO;
import dto.GenreDTO;
import metier.DAOMetier;
import metier.IDAOMetier;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.util.*;
import java.util.List;

public class CatalogueParGenre {
    //Lien avec la couche metier
    private IDAOMetier cinema;

    private JPanel rootPanel;
    private JList listGenre;
    private JList <FilmDTO> listFilm;
    private JTextArea textAreaInfoFilm;
    private JTextArea textAreaNbreFilm;
    private JPanel test;
    private JPanel test1;
    private JScrollPane jscroll1;

    private Color sombre = new Color(89,91,93);
    private Color clair = new Color(175,177,179);


    private void init(){
        this.rootPanel.setBackground(sombre);
        this.textAreaNbreFilm.setBackground(clair);
        this.textAreaInfoFilm.setBackground(clair);
        this.listGenre.setBackground(clair);
        this.listFilm.setBackground(clair);
        this.test.setBackground(clair);
        this.test1.setBackground(clair);


        try{
            //lien avec la bse de données
            cinema = DAOMetier.getInstance();
            final List<GenreDTO> genres = cinema.ensGenres();

            DefaultListModel<GenreDTO> lm = new DefaultListModel();
            for(GenreDTO numGenre : genres){
                lm.addElement(numGenre);
            }

            listGenre.setModel(lm);

            this.textAreaNbreFilm.setText("Nombre de film du genre : ");
        }catch (Exception e){
            System.err.println(e.getMessage());
        }
    }





    public CatalogueParGenre(){
        init();

        //--------------------- S E L E C T I O N   D U   G E N R E  ------------------------------------
        listGenre.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                try {
                    //recupereation de la legende selectionné
                    GenreDTO selection = (GenreDTO) listGenre.getSelectedValue();
                    //reaction 1 --> maj du text
                    textAreaNbreFilm.setText("Nombre de film du genre "+selection+" : "+cinema.nbreFilmDuGenre(selection.getNgenre()));

                    //reaction 2 --> maj du text real
                    textAreaInfoFilm.setText("");

                    //reaction 3
                    final List<FilmDTO> films = cinema.ensFilmsDuGenre(selection.getNgenre());
                    ListModel<FilmDTO> lm = new AbstractListModel(){
                        public int getSize(){
                            return films.size();
                        }
                        public FilmDTO getElementAt(int index){
                            return films.get(index);
                        }
                    };
                    listFilm.setModel(lm);

                }catch(Exception e1){
                    System.err.println(e1.getMessage());
                }

            }
        });
        listFilm.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent listSelectionEvent) {
                //recuperation de la selction
                FilmDTO selection = listFilm.getSelectedValue();

                textAreaInfoFilm.setText("Acteur principal : "+ selection.getNomActeur());
                textAreaInfoFilm.append("\nRéalisteur : " + selection.getNomRealisateur());
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("CatalogueParGenre");
        frame.setContentPane(new CatalogueParGenre().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
