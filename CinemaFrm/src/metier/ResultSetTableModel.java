package metier;


import java.sql.*;
import javax.swing.table.*;


abstract class ResultSetTableModel extends AbstractTableModel {
    private ResultSet rs;
    private ResultSetMetaData rsmd;

    public ResultSetTableModel(ResultSet aResultSet) {
        this.rs = aResultSet;
        try {
            rsmd = rs.getMetaData();
        } catch (SQLException e) {
            System.out.println("Erreur " + e);
        }
    }

    @Override
    public String getColumnName(int column) {
        try {
            return rsmd.getColumnName(column + 1);
        } catch (SQLException e) {
            System.out.println("Erreur " + e);
            return "";
        }
    }

    @Override
    public int getColumnCount() {
        try {
            return rsmd.getColumnCount();
        } catch (SQLException e) {
            System.out.println("Erreur " + e);
            return 0;
        }
    }

    protected ResultSet getResultSet() {
        return rs;
    }
}
