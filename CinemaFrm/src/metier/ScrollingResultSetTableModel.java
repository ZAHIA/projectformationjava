package metier;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ScrollingResultSetTableModel extends ResultSetTableModel {
    public ScrollingResultSetTableModel(ResultSet aResultSet) {
        super(aResultSet);
    }

    public Object getValueAt(int r, int c)
    {
        try {
            ResultSet rs = getResultSet();
            rs.absolute(r + 1);
            return rs.getObject(c + 1);
        } catch (SQLException e) {
            System.out.println("Erreur " + e);
            return null;
        }
    }

    public int getRowCount()
    {
        try {
            ResultSet rs = getResultSet();
            rs.last();
            return rs.getRow();
        } catch (SQLException e) {
            System.out.println("Erreur " + e);
            return 0;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex){
        return false;
    }
}

