<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 19/02/2020
  Time: 15:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"
    import="java.util.*, java.text.SimpleDateFormat"
%>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <h1>TP1</h1>
  <p>
    Mois :
    <%
      // ------ NE PAS OUBLIER D'APPELER LA BIBLIOTHEQUE JAVA.UTIL.* -------
      //Metier
      Calendar maintenant = Calendar.getInstance();
      int nb = maintenant.get(Calendar.MONTH) + 1;

      //Affichage resultat
      out.println(nb);
      System.out.println("numero du prochain mois : " + nb);
    %>
  </p>
  <p>
    Date du jour :
    <%
      SimpleDateFormat formatter = new SimpleDateFormat("dd-MMMM-yyyy HH:mm:ss");
      Date now = maintenant.getTime();
      out.println(formatter.format(now));
    %>

  </p>
  </body>
</html>
