package beans;

import db_util.MyConnection;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ControlBean implements Serializable {
    private static final long serialVersionUID = 1L;

    //CORRESPOND AU PARAMETRE DES DEUX FORMULAIRES HTML
    //FORMULAIRE ENREGISTREMENT INCIDENT
    private String email;
    private String software;
    private String os;
    private String problem;

    //FORMAULAIRE ENREGISTREMENT CLIENT
    private String firstname;
    private String lastname;
    private String phoneNumber;

    //REQUETES
    private static final String insertStatementStr =
            "INSERT INTO CUSTOMERS VALUES(?, ?, ?, ? )";
    private static final String selectCustomerStr =
            "SELECT fname, lname, phone " +
                    " FROM CUSTOMERS " +
                    " WHERE LOWER(email) = ? ";
    private static final String insertSuppRequestStr =
            " INSERT INTO SUPP_REQUESTS(email, software, os, problem) " +
                    " VALUES(?, ?, ?, ? )";

    public ControlBean(){

    }

    //GETTER SETTER

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase();
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    //REGLES METIER
    //ACCES A LA BASE DE DONNEES
    public void registerSupportRequest() throws SQLException {
        PreparedStatement insertStatement =
                MyConnection.getINSTANCE().prepareStatement(insertSuppRequestStr);

        insertStatement.setString(1,email.toLowerCase());
        insertStatement.setString(2,software);
        insertStatement.setString(3,os);
        insertStatement.setString(4,problem);

        insertStatement.executeUpdate();

        //on verifie si le client est enregistré ou non
        PreparedStatement selectStatement = MyConnection.getINSTANCE().prepareStatement(selectCustomerStr);
        selectStatement.setString(1,this.email.toLowerCase());

        ResultSet rs = selectStatement.executeQuery();

        if (rs.next()){
            System.out.println("le client est enregistre");
            this.setFirstname((rs.getString("fname")));
            this.setLastname(rs.getString("lname"));
            this.setPhoneNumber(rs.getString("phone"));

            //Le client est enregistre on va a la page reponse
            this.registered = true;
        }else{
            //le client n'est pas encore enregistre on doit aller au formulaire
            this.registered = false;
        }

    }

    public void registerCustomer() throws SQLException{
        PreparedStatement insertStatement = null;
        try {
            MyConnection.getINSTANCE().setAutoCommit(false);
            insertStatement = MyConnection.getINSTANCE().prepareStatement(insertStatementStr);

            insertStatement.setString(1,email.toLowerCase());
            insertStatement.setString(2,firstname);
            insertStatement.setString(3,lastname);
            insertStatement.setString(4,phoneNumber);

            insertStatement.executeUpdate();
            MyConnection.getINSTANCE().commit();
        }catch (SQLException ex){
            MyConnection.getINSTANCE().rollback();
            throw ex;
        }
    }

    private boolean registered;

    public boolean isRegistered(){return registered;}


    public static void main(String[] args) {
        ControlBean cb = new ControlBean();
        cb.setEmail("test@test.fr");
        cb.setOs("ubuntu");
        cb.setSoftware("intellij");
        cb.setProblem("merde");

    }


}
