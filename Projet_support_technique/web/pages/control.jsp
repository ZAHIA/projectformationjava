
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 20/02/2020
  Time: 12:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java"   import="beans.*" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- CREATION ET INIT DU COMPOSANT METIER --%>
<jsp:useBean id="techSupportBean"
             scope="session"
             class="beans.ControlBean">
    <jsp:setProperty name="techSupportBean" property="*"/>
</jsp:useBean>

<%-- REGLE METIER : ENREGISTREMENT INCIDENT --%>
<%
    techSupportBean.registerSupportRequest();
%>

<%-- REGLE METIER LE CLIENT EXISTE T-IL EN BD ? isregistered--%>
<c:if test="${techSupportBean.registered}">
    <jsp:forward page="response.jsp"/>
</c:if>

<jsp:forward page="regForm.jsp"/>