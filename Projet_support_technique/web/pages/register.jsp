<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 20/02/2020
  Time: 14:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" errorPage="error.jsp" import="beans.*" %>

<%-- Locallisation du composant metier --%>
<jsp:useBean id="techSupportBean"
             scope="session"
             class="beans.ControlBean">
</jsp:useBean>

<%-- mise a joiur des 3 champs lnmae, fname, phone du composant metier --%>
<jsp:setProperty name="techSupportBean" property="*"/>

<%-- REGLE METIER : ENREGISTREMENT CLIENT --%>
<% techSupportBean.registerCustomer();%>

<%-- cinematique --%>
<jsp:forward page="response.jsp"/>