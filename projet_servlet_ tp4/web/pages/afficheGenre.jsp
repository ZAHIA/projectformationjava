<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: michael
  Date: 24/02/2020
  Time: 15:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Vue choix</title>
</head>
<body>
<form method="POST" action="process?action=filmsDuGenre">
    <fieldset>
        <legend> choix d'un genre</legend>
        Genre:
        <select name="genre">
            <c:forEach var="genre" items="${ensGenres}">
                <option value="${genre.ngenre}">
                    ${genre.nature}
                </option>
            </c:forEach>
        </select>
        <input type="submit" value="envoie">
    </fieldset>
</form>
</body>
</html>
