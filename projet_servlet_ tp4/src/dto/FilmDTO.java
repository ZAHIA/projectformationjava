package dto;

import java.io.Serializable;

public class FilmDTO implements Serializable {

    // ---------------------attributs d'instance-------------------------
    private static final long serialVersionUID = 1L;

    private int nfilm ;
    private String titre;
    private String nomRealisateur;
    private String nomActeur;

    // ---------------------------- G E T T E R --------------------------
    public int getNfilm() {
        return nfilm;
    }

    public String getTitre() {
        return titre;
    }

    public String getNomRealisateur() {
        return nomRealisateur;
    }

    public String getNomActeur() {
        return nomActeur;
    }

    // ----------------------- C O N S T R U C T E U R ---------------------------------
    public FilmDTO(int nfilm, String titre, String nomRealisateur, String nomActeur) {
        super();
        this.nfilm = nfilm;
        this.titre = titre;
        this.nomRealisateur = nomRealisateur;
        this.nomActeur = nomActeur;
    }

    //--------------------------- T O S T R I N G ---------------------------------------


    @Override
    public String toString() {
        return   titre;
    }
}
