package dto;

import java.io.Serializable;

public class GenreDTO implements Serializable {

    private static final long serialVersionUID = 1L;
    private int ngenre;
    private String nature;

    //GETTER SETTER
    public int getNgenre() {
        return ngenre;
    }

    public void setNgenre(int ngenre) {
        this.ngenre = ngenre;
    }

    public String getNature() {
        return nature;
    }

    public void setNature(String nature) {
        this.nature = nature;
    }

    public GenreDTO(int ngenre, String nature) {
        this.ngenre = ngenre;
        this.nature = nature;
    }
}
