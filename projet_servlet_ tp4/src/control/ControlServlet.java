package control;

import dao.CinemaDAO;
import dto.FilmDTO;
import dto.GenreDTO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;
import java.io.IOException;

@WebServlet("/process")
public class ControlServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRquest(request, response);
    }

    //navigation
    private void forward(String url, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        RequestDispatcher rd = request.getRequestDispatcher(url);
        rd.forward(request, response);
    }

    //routage
    private void processRquest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        try {
            //recupertaion de l'action
            String action = request.getParameter("action");

            //execution du traitement
            if (action.equals("init")){
                doInit(request, response);
            }else if (action.equals("filmsDuGenre")){
                doFilmsDuGenre(request, response);
            }else if (action.equals("choix")){
                doFilmsSelectionnes(request, response);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void doInit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //creation du DAO si necessaire
        CinemaDAO dao = CinemaDAO.getInstance();

        //interrogation du modele afin de trouver l'ensemble des genres
        List<GenreDTO> ensGenres = dao.findAllGenres();

        //sauvegarde de cet ensemmble dans le scop request
        request.setAttribute("ensGenres", ensGenres);
        System.out.println(ensGenres);

        //alternative  pour sauvegarder l'ens des genres de session
        //request.getSession().setAttribute("ensGenres", ensGenres);

        //prochaine vue
        String url = "pages/afficheGenre.jsp";
        forward(url, request, response);
    }

    private void doFilmsDuGenre(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //recuperation du numero du film du genre choisi
        String choix = request.getParameter("genre");
        int ngenre = Integer.parseInt(choix);

        //interrogation du model afin de recuperer les film de cet acteur ainsi que l'acteur
        CinemaDAO dao = CinemaDAO.getInstance();
        List<FilmDTO> ensFilms = dao.findFilmsOfGenre(ngenre);
        GenreDTO genre = dao.findGenreById(ngenre);

        //sauvegarde dans le scope request
        request.setAttribute("genre", genre);
        request.setAttribute("ensFilms", ensFilms);
        System.out.println(ensFilms);

        //cinematique
        String url = "pages/afficheFilmsDuGenre.jsp";
        forward(url, request, response);
    }

    private void doFilmsSelectionnes(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //recuperation
        String[] choix = request.getParameterValues("choix");

        //creation du panier
        CinemaDAO dao = CinemaDAO.getInstance();
        HttpSession session = request.getSession();
        Map<String, FilmDTO> ensChoisis = (HashMap<String, FilmDTO>)session.getAttribute("ensChoix");
        if(ensChoisis == null){
            ensChoisis = new HashMap<>();
        }

        for (String cle : choix){
            int nfilm=Integer.parseInt(cle);
            FilmDTO film = dao.findFilmById(nfilm);
            ensChoisis.put(cle, film);
        }

        //sauvegarde
        synchronized (session){
            session.setAttribute("ensChoix", ensChoisis);
        }

        //cinematique vers la vue
        String url = "pages/afficheFilmsChoisis.jsp";
        forward(url, request, response);
    }

}
